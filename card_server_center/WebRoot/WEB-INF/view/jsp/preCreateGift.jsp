<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'preCreateGift.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script language="javascript" type="text/javascript" src="datepicker/WdatePicker.js" charset="gb2312" ></script>
	<script type="text/javascript">
		function check()
		{
			var name=document.getElementById("name");
			if(name.value==""){
				alert("礼包名称不能为空!");
				name.focus();
				return;
			}
			var num=document.getElementById("num");
			if(!(/(^[1-9]\d*$)/.test(num.value)))
			{
				alert("输入有误，请输入正整数！")
				num.focus();
				return;
			}
			var gold=document.getElementById("gold");
			if(!(/(^[1-9]\d*|0$)/.test(gold.value)))
			{
				alert("输入有误，请输入正整数！")
				gold.focus();
				return;
			}
			var crystal=document.getElementById("crystal");
			if(!(/(^[1-9]\d*|0$)/.test(crystal.value)))
			{
				alert("输入有误，请输入正整数！")
				crystal.focus();
				return;
			}
			var runeNum=document.getElementById("runeNum");
			if(!(/(^[1-9]\d*|0$)/.test(runeNum.value)))
			{
				alert("输入有误，请输入正整数！")
				runeNum.focus();
				return;
			}
			var power=document.getElementById("power");
			if(!(/(^[1-9]\d*|0$)/.test(power.value)))
			{
				alert("输入有误，请输入正整数！")
				power.focus();
				return;
			}
			document.form1.submit();
		}
	</script>
  </head>

  <body>
    <form action="user.htm?action=createGift" method="post" name="form1">
	<table width="90%" border="1" cellspacing="0" cellpadding="0" align="center">
	  <tr align="center">
        <td colspan="2">
           <strong>添加礼包</strong>
           <font color="red">
           <%
			String msg=(String)request.getAttribute("result");
			if(msg!=null)
			{
				out.print(msg);
			}
			%>
			</font>
		</td>
	  </tr>
	  <tr>
        <td width="20%" height="30" align="right" class="table_g3">礼包名称：</td>
	    <td width="80%" class="table_w2" align="left">　
			<input type="text" id="name" name="name" maxlength="60">(最多60字符)
	    </td>
	  </tr>
	  <tr>
        <td width="20%" height="30" align="right" class="table_g3">激活码数量：</td>
	    <td width="80%" class="table_w2" align="left">　
			<input type="text" id="num" name="num" value="1000" class="{required:true,digits:true,range:[1,5000]}" onkeyup="checkNum();">(单次创建上限为5000)
	    </td>
	  </tr>
	  <tr>
        <td width="20%" height="30" align="right" class="table_g3">有效时间：</td>
	    <td width="80%" class="table_w2" align="left">　
		    <input type="text" name="startDate" id="startDate" class="Wdate" onFocus="new WdatePicker(this,'%Y-%M-%D',false)"/>
			<span class="newfont06">至</span>
			<input type="text" name="endDate" id="endDate" class="Wdate" onFocus="new WdatePicker(this,'%Y-%M-%D',false)"/>
	    </td>
	  </tr>
	  <tr>
        <td width="20%" height="30" align="right" class="table_g3">金币：</td>
	    <td width="80%" class="table_w2" align="left">　
			<input type="text" id="gold" name="gold" value="0" class="{digits:true,rangelength:[1,8]}" maxlength="8">
	    </td>
	  </tr>
	  <tr>
        <td width="20%" height="30" align="right" class="table_g3">水晶：</td>
	    <td width="80%" class="table_w2" align="left">　
			<input type="text" id="crystal" name="crystal" value="0" class="{digits:true,rangelength:[1,8]}" maxlength="8">
	    </td>
	  </tr>
	  <tr>
        <td width="20%" height="30" align="right" class="table_g3">符文值：</td>
	    <td width="80%" class="table_w2" align="left">　
			<input type="text" id="runeNum" name="runeNum" value="0" class="{digits:true,rangelength:[1,8]}" maxlength="8">
	    </td>
	  </tr>
	  <tr>
        <td width="20%" height="30" align="right" class="table_g3">体力：</td>
	    <td width="80%" class="table_w2" align="left">　
			<input type="text" id="power" name="power" value="0" class="{digits:true,rangelength:[1,8]}" maxlength="8">
	    </td>
	  </tr>
	  <tr>
        <td width="20%" height="30" align="right" class="table_g3">英雄卡(ID)：</td>
	    <td width="80%" class="table_w2" align="left">　
			<input type="text" id="cardId" name="cardId" maxlength="2000" size="80"><font color="red">*格式：英雄卡id-数量 ,多组之间用英文","号分割</font>
	    </td>
	  </tr>
	  <tr>
        <td width="20%" height="30" align="right" class="table_g3">主动技能(ID)：</td>
	    <td width="80%" class="table_w2" align="left">　
			<input type="text" id="skillId" name="skillId" maxlength="2000" size="80"><font color="red">*格式：主动技能id-数量 ,多组之间用英文","号分割</font>
	    </td>
	  </tr>
	  <tr>
        <td width="20%" height="30" align="right" class="table_g3">被动技能(ID)：</td>
	    <td width="80%" class="table_w2" align="left">　
			<input type="text" id="pSkillId" name="pSkillId" maxlength="2000" size="80"><font color="red">*格式：被动技能id-数量 ,多组之间用英文","号分割</font>
	    </td>
	  </tr>
	  <tr>
        <td width="20%" height="30" align="right" class="table_g3">装备(ID)：</td>
	    <td width="80%" class="table_w2" align="left">　
			<input type="text" id="equipId" name="equipId" maxlength="2000" size="80"><font color="red">*格式：装备id-数量 ,多组之间用英文","号分割</font>
	    </td>
	  </tr>
	  <tr>
	    <td width="20%" height="30" align="right" class="table_g3">材料(ID)：</td>
	    <td width="80%" class="table_w2" align="left">　
			<input type="text" id="itemId" name="itemId" maxlength="2000" size="80"><font color="red">*格式：材料id-数量 ,多组之间用英文","号分割</font>
	    </td>
	  </tr>
	  <tr>
        <td height="30" align="center" class="table_g3" colspan="2">
			<input type="button" value="添加" onclick="javascript:check();"/>
		</td>
	  </tr>
    </table>
	</form>
  </body>
</html>
