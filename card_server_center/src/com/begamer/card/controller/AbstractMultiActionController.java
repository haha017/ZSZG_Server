package com.begamer.card.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.begamer.card.common.Constant;
import com.begamer.card.model.pojo.Manager;

/**
 * 
 * @ClassName: AbstractMultiActionController
 * @Description: TODO
 * @author gs
 * @date Nov 1, 2011 1:43:48 PM
 * 
 */
public abstract class AbstractMultiActionController extends MultiActionController {
	
	/**
	 * 获取当前GM登录用户
	 * @param request
	 * @return
	 */
	public Manager getManager(HttpServletRequest request)
	{
		Manager manager = (Manager) request.getSession().getAttribute(Constant.LOGIN_USER);
		return manager;
	}
}
