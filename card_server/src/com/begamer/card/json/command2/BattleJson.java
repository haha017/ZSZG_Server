package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;


public class BattleJson extends BasicJson
{	
	/**targetId:关卡Id,pvpId**/
	public int td;
	/**helpPlayerId:援护玩家**/
	public int h;
	
	public int getTd()
	{
		return td;
	}
	public void setTd(int td)
	{
		this.td = td;
	}
	public int getH()
	{
		return h;
	}
	public void setH(int h)
	{
		this.h = h;
	}
	
}
