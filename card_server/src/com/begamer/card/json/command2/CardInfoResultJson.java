package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.PackElement;

public class CardInfoResultJson extends ErrorJson {
	public int ps1;// 30级解锁的被动技能 ---是否有新提醒 ,0无提醒，1有提醒
	public int ps2;// 40级解锁的被动技能 ---是否有新提醒 ,0无提醒，1有提醒
	public int ps3;// 55级解锁的被动技能 ---是否有新提醒 ,0无提醒，1有提醒
	public int equip1;// 武器 ----0无提醒，1有提醒，2提升
	public int equip2;// 防具 ----0无提醒，1有提醒，2提升
	public int equip3;// 饰品 ----0无提醒，1有提醒，2提升
	public int diamond;// 突破需要金罡心数量
	public int pd;//玩家当前拥有的金罡心
	public int cardN;// 突破需要卡牌数量
	public int pCardN;// 玩家拥有此卡牌的数量
	public int multCard;// 玩家拥有同星级的万能突破卡
	public List<PackElement> pes;// 突破卡牌索引
	
	public int getPs1()
	{
		return ps1;
	}
	
	public void setPs1(int ps1)
	{
		this.ps1 = ps1;
	}
	
	public int getPs2()
	{
		return ps2;
	}
	
	public void setPs2(int ps2)
	{
		this.ps2 = ps2;
	}
	
	public int getPs3()
	{
		return ps3;
	}
	
	public void setPs3(int ps3)
	{
		this.ps3 = ps3;
	}
	
	public int getEquip1()
	{
		return equip1;
	}
	
	public void setEquip1(int equip1)
	{
		this.equip1 = equip1;
	}
	
	public int getEquip2()
	{
		return equip2;
	}
	
	public void setEquip2(int equip2)
	{
		this.equip2 = equip2;
	}
	
	public int getEquip3()
	{
		return equip3;
	}
	
	public void setEquip3(int equip3)
	{
		this.equip3 = equip3;
	}
	
	public int getDiamond()
	{
		return diamond;
	}
	
	public void setDiamond(int diamond)
	{
		this.diamond = diamond;
	}
	
	public int getCardN()
	{
		return cardN;
	}
	
	public void setCardN(int cardN)
	{
		this.cardN = cardN;
	}
	
	public int getPCardN()
	{
		return pCardN;
	}
	
	public void setPCardN(int pCardN)
	{
		this.pCardN = pCardN;
	}
	
	public int getMultCard()
	{
		return multCard;
	}
	
	public void setMultCard(int multCard)
	{
		this.multCard = multCard;
	}
	
	public List<PackElement> getPes()
	{
		return pes;
	}
	
	public void setPes(List<PackElement> pes)
	{
		this.pes = pes;
	}

	public int getPd()
	{
		return pd;
	}

	public void setPd(int pd)
	{
		this.pd = pd;
	}
	
}
