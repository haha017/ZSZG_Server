package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class PayOrderJson extends BasicJson
{
	public String consumValue;
	public String extra;
	
	public String getConsumValue()
	{
		return consumValue;
	}
	public void setConsumValue(String consumValue)
	{
		this.consumValue = consumValue;
	}
	public String getExtra()
	{
		return extra;
	}
	public void setExtra(String extra)
	{
		this.extra = extra;
	}
}
