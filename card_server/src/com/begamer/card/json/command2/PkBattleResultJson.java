package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.CardJson;
import com.begamer.card.json.ErrorJson;

public class PkBattleResultJson extends ErrorJson
{
	public CardJson[] cs0;//cards0//
	public CardJson[] cs1;//cards1//
	public int[] us0;//unitSkillsId0//
	public int[] us1;//unitSkillsId1//
	public int bNum;//战斗场次
	public int pkId;
	public int[] mes;//==双方最大怒气上限==//
	public int[] bps;//==双方战力,战力大的一方先出手==//
	public List<String> ds;//物品掉落id-个数
	public String[] ns;//==双方名字==//
	public int[] lvs;//==双方等级==//
	
	public String [] runes;//==双方符文id==//
	public String[] raceAtts1;//==己方种族加成属性==//
	public String[] raceAtts2;//==对方种族加成属性==//
	public int[] initEs;//==初始怒气==//
	
//	public int pvpAward;//pvp每日奖励
	public CardJson[] getCs0() {
		return cs0;
	}
	public void setCs0(CardJson[] cs0) {
		this.cs0 = cs0;
	}
	public CardJson[] getCs1() {
		return cs1;
	}
	public void setCs1(CardJson[] cs1) {
		this.cs1 = cs1;
	}
	public int[] getUs0() {
		return us0;
	}
	public void setUs0(int[] us0) {
		this.us0 = us0;
	}
	public int[] getUs1() {
		return us1;
	}
	public void setUs1(int[] us1) {
		this.us1 = us1;
	}
	public int getBNum() {
		return bNum;
	}
	public void setBNum(int num) {
		bNum = num;
	}
	public int getPkId() {
		return pkId;
	}
	public void setPkId(int pkId) {
		this.pkId = pkId;
	}
	public int[] getMes()
	{
		return mes;
	}
	public void setMes(int[] mes)
	{
		this.mes = mes;
	}
	public int[] getBps()
	{
		return bps;
	}
	public void setBps(int[] bps)
	{
		this.bps = bps;
	}
	public List<String> getDs() {
		return ds;
	}
	public void setDs(List<String> ds) {
		this.ds = ds;
	}
	public String[] getNs()
	{
		return ns;
	}
	public void setNs(String[] ns)
	{
		this.ns = ns;
	}
	public int[] getLvs()
	{
		return lvs;
	}
	public void setLvs(int[] lvs)
	{
		this.lvs = lvs;
	}
	public String[] getRunes() {
		return runes;
	}
	public void setRunes(String[] runes) {
		this.runes = runes;
	}
	public String[] getRaceAtts1()
	{
		return raceAtts1;
	}
	public void setRaceAtts1(String[] raceAtts1)
	{
		this.raceAtts1 = raceAtts1;
	}
	public String[] getRaceAtts2()
	{
		return raceAtts2;
	}
	public void setRaceAtts2(String[] raceAtts2)
	{
		this.raceAtts2 = raceAtts2;
	}
	public int[] getInitEs()
	{
		return initEs;
	}
	public void setInitEs(int[] initEs)
	{
		this.initEs = initEs;
	}
	
}
