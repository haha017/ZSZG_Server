package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;

public class ActiveResultJson extends ErrorJson {
	
	private String activeState;//请求后的礼包状态
	public String getActiveState() {
		return activeState;
	}

	public void setActiveState(String activeState) {
		this.activeState = activeState;
	}
	
}
