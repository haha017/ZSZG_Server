package com.begamer.card.model.view;

public class ItemView {

	private int id;
	private String name;
	private int pile;

	public static ItemView createItemView(int id,String name,int pile)
	{
		ItemView itemView = new ItemView();
		itemView.setId(id);
		itemView.setName(name);
		itemView.setPile(pile);
		return itemView;
	}
	
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getPile()
	{
		return pile;
	}

	public void setPile(int pile)
	{
		this.pile = pile;
	}
}
