package com.begamer.card.model.pojo;

public class KopointLog {

	public int id;
	public int playerId;
	public int kopointNum;
	public String logDate;
	
	public static KopointLog createKopointLog(int playerId,int kopointNum,String logDate)
	{
		KopointLog kopointLog = new KopointLog();
		kopointLog.setPlayerId(playerId);
		kopointLog.setKopointNum(kopointNum);
		kopointLog.setLogDate(logDate);
		return kopointLog;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPlayerId() {
		return playerId;
	}
	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}

	public int getKopointNum() {
		return kopointNum;
	}

	public void setKopointNum(int kopointNum) {
		this.kopointNum = kopointNum;
	}

	public String getLogDate() {
		return logDate;
	}
	public void setLogDate(String logDate) {
		this.logDate = logDate;
	}
	
	
}
