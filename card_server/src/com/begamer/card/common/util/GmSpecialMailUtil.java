package com.begamer.card.common.util;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.begamer.card.cache.Cache;
import com.begamer.card.common.dao.hibernate.CommDao;
import com.begamer.card.log.PlayerLogger;
import com.begamer.card.model.pojo.ConsumeCrystalLog;
import com.begamer.card.model.pojo.KopointLog;
import com.begamer.card.model.pojo.LotLog;
import com.begamer.card.model.pojo.Mail;
import com.begamer.card.model.pojo.Player;
import com.begamer.card.model.pojo.SpeciaMail;

public class GmSpecialMailUtil {
	
	private static GmSpecialMailUtil instance;
	
	public void openThisClass()
	{
		if (instance == null)
		{
			instance = this;
		}
	}
	
	public static GmSpecialMailUtil getInstance()
	{
		return instance;
	}
	
	private GmSpecialMailUtil()
	{
	};
	
	private static final Logger playerLogger = PlayerLogger.logger;
	
	@Autowired
	private CommDao commDao;
	
	/** 消耗钻石发送奖励 **/
	public void saveConsumeCrystalLog(int id, String name, int level, int num)
	{
		// 保存消耗钻石信息
		ConsumeCrystalLog cLog = ConsumeCrystalLog.createConsumeCrystalLog(id, num, StringUtil.getDate(System.currentTimeMillis()));
		commDao.saveConsumeCrystalLog(cLog);
		// 累计消耗n钻石发奖励
		totalRemoveCrystalSpeciaMail(id, name, level, cLog.getId());
		// 单笔消耗n钻石发奖励
		oneRemoveCrystalSpeciaMail(id, name, level, num);
	}
	
	// 累计消耗n钻石发奖励
	private void totalRemoveCrystalSpeciaMail(int id, String name, int level, int cid)
	{
		List<SpeciaMail> speciaMails = commDao.getSpeciaMails(5);
		if (speciaMails != null && speciaMails.size() > 0)
		{
			long curTime = System.currentTimeMillis();
			for (int k = 0; k < speciaMails.size(); k++)
			{
				SpeciaMail speciaMail1 = speciaMails.get(k);
				if (speciaMail1.isTrue(curTime))
				{
					int curConsumeNums = commDao.getConsumeCrystalTotal(0, id, speciaMail1.getBeginTime(), speciaMail1.getEndTime());
					int consumeNums = commDao.getConsumeCrystalTotal(cid, id, speciaMail1.getBeginTime(), speciaMail1.getEndTime());
					if (curConsumeNums >= speciaMail1.getValue() && consumeNums < speciaMail1.getValue())
					{
						Mail mail = Mail.createMail(id, "GM", speciaMail1.getTitle(), speciaMail1.getContent(), speciaMail1.getReward1(), speciaMail1.getReward2(), speciaMail1.getReward3(), speciaMail1.getReward4(), speciaMail1.getReward5(), speciaMail1.getReward6(), speciaMail1.getGold(), speciaMail1.getCrystal(), speciaMail1.getRuneNum(), speciaMail1.getPower(), speciaMail1.getFriendNum(), speciaMail1.getDeleteTime());
						Cache.getInstance().sendMail(mail);
						playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + id + "|" + name + "|等级：" + level + "|获得累计消耗" + speciaMail1.getValue() + "钻石,奖励邮件");
					}
				}
			}
		}
	}
	
	// 单笔消耗n钻石发奖励
	private void oneRemoveCrystalSpeciaMail(int id, String name, int level, int num)
	{
		List<SpeciaMail> speciaMails = commDao.getSpeciaMails(6);
		if (speciaMails != null && speciaMails.size() > 0)
		{
			long curTime = System.currentTimeMillis();
			for (int k = 0; k < speciaMails.size(); k++)
			{
				SpeciaMail speciaMail1 = speciaMails.get(k);
				if (speciaMail1.isTrue(curTime))
				{
					if (num >= speciaMail1.getValue())
					{
						Mail mail = Mail.createMail(id, "GM", speciaMail1.getTitle(), speciaMail1.getContent(), speciaMail1.getReward1(), speciaMail1.getReward2(), speciaMail1.getReward3(), speciaMail1.getReward4(), speciaMail1.getReward5(), speciaMail1.getReward6(), speciaMail1.getGold(), speciaMail1.getCrystal(), speciaMail1.getRuneNum(), speciaMail1.getPower(), speciaMail1.getFriendNum(), speciaMail1.getDeleteTime());
						Cache.getInstance().sendMail(mail);
						playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + id + "|" + name + "|等级：" + level + "|获得单笔消耗" + speciaMail1.getValue() + "钻石,奖励邮件");
					}
				}
			}
		}
	}
	
	/** 玩家等级达到多少发送奖励 **/
	public void playerLevelSpecialMail(int id, String name, int level)
	{
		List<SpeciaMail> speciaMails = commDao.getSpeciaMails(7);
		if (speciaMails != null && speciaMails.size() > 0)
		{
			long curTime = System.currentTimeMillis();
			for (int k = 0; k < speciaMails.size(); k++)
			{
				SpeciaMail speciaMail1 = speciaMails.get(k);
				if (speciaMail1.isTrue(curTime))
				{
					if (level == speciaMail1.getValue())
					{
						Mail mail = Mail.createMail(id, "GM", speciaMail1.getTitle(), speciaMail1.getContent(), speciaMail1.getReward1(), speciaMail1.getReward2(), speciaMail1.getReward3(), speciaMail1.getReward4(), speciaMail1.getReward5(), speciaMail1.getReward6(), speciaMail1.getGold(), speciaMail1.getCrystal(), speciaMail1.getRuneNum(), speciaMail1.getPower(), speciaMail1.getFriendNum(), speciaMail1.getDeleteTime());
						Cache.getInstance().sendMail(mail);
						playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + id + "|" + name + "|等级：" + level + "|获得等级达到" + speciaMail1.getValue() + "级,奖励邮件");
					}
				}
			}
		}
	}
	
	/** 玩家战力达到n发奖励 **/
	public void playerBattlePowerSpeciaMail(Player player)
	{
		List<SpeciaMail> speciaMails = commDao.getSpeciaMails(8);
		if (speciaMails != null && speciaMails.size() > 0)
		{
			long curTime = System.currentTimeMillis();
			for (int k = 0; k < speciaMails.size(); k++)
			{
				SpeciaMail speciaMail1 = speciaMails.get(k);
				if (speciaMail1.isTrue(curTime))
				{
					if (player.getBattlePower() >= speciaMail1.getValue())
					{
						String isNull = player.getBattlepowerSpeciamail();
						boolean is = true;
						if (isNull != null && isNull.length() > 0)
						{
							for (String iszu : isNull.split(","))
							{
								if (StringUtil.getInt(iszu) == speciaMail1.getId())
								{
									is = false;
									break;
								}
							}
						}
						if (is)
						{
							StringBuilder str = new StringBuilder();
							str.append(player.getBattlepowerSpeciamail() + speciaMail1.getId() + ",");
							player.setBattlepowerSpeciamail(str.toString().trim());
							Mail mail = Mail.createMail(player.getId(), "GM", speciaMail1.getTitle(), speciaMail1.getContent(), speciaMail1.getReward1(), speciaMail1.getReward2(), speciaMail1.getReward3(), speciaMail1.getReward4(), speciaMail1.getReward5(), speciaMail1.getReward6(), speciaMail1.getGold(), speciaMail1.getCrystal(), speciaMail1.getRuneNum(), speciaMail1.getPower(), speciaMail1.getFriendNum(), speciaMail1.getDeleteTime());
							Cache.getInstance().sendMail(mail);
							playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + player.getId() + "|" + player.getName() + "|等级：" + player.getLevel() + "|获得战力达到" + speciaMail1.getValue() + ",奖励邮件");
						}
					}
				}
			}
		}
	}
	
	/** 存活动期间kopointLog **/
	public void saveKopointLog(int playerId, String name, int level, int kopoint)
	{
		KopointLog kopointLog = null;
		List<SpeciaMail> speciaMails = commDao.getSpeciaMails(9);
		long curTime = System.currentTimeMillis();
		boolean is = false;
		if (speciaMails != null && speciaMails.size() > 0)
		{
			for (SpeciaMail speciaMail : speciaMails)
			{
				if (speciaMail.isTrue(curTime))
				{
					is = true;
					break;
				}
			}
		}
		if (is)
		{
			kopointLog = KopointLog.createKopointLog(playerId, kopoint, StringUtil.getDate(curTime));
			commDao.saveKopointLog(kopointLog);
			// ko积分达到n次发奖励
			kopointSpeciaMail(kopointLog.getId(), curTime, speciaMails, playerId, name, level);
		}
	}
	
	/** KO积分达到n发奖励 **/
	public void kopointSpeciaMail(int kid, long curTime, List<SpeciaMail> speciaMails, int id, String name, int level)
	{
		if (speciaMails != null && speciaMails.size() > 0)
		{
			for (int k = 0; k < speciaMails.size(); k++)
			{
				SpeciaMail speciaMail1 = speciaMails.get(k);
				if (speciaMail1.isTrue(curTime))
				{
					int curKopointNums = commDao.getKopointLogByDate(0, id, speciaMail1.getBeginTime(), speciaMail1.getEndTime());
					int allKopointNums = commDao.getKopointLogByDate(kid, id, speciaMail1.getBeginTime(), speciaMail1.getEndTime());
					if (curKopointNums >= speciaMail1.getValue() && allKopointNums < speciaMail1.getValue())
					{
						Mail mail = Mail.createMail(id, "GM", speciaMail1.getTitle(), speciaMail1.getContent(), speciaMail1.getReward1(), speciaMail1.getReward2(), speciaMail1.getReward3(), speciaMail1.getReward4(), speciaMail1.getReward5(), speciaMail1.getReward6(), speciaMail1.getGold(), speciaMail1.getCrystal(), speciaMail1.getRuneNum(), speciaMail1.getPower(), speciaMail1.getFriendNum(), speciaMail1.getDeleteTime());
						Cache.getInstance().sendMail(mail);
						playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + id + "|" + name + "|等级：" + level + "|获得Kopoint" + speciaMail1.getValue() + ",奖励邮件");
					}
				}
			}
		}
	}
	
	/** 存活动期间玩家十连抽记录 **/
	public void saveChouCardLog(int playerId, String name, int level)
	{
		LotLog chouCardLog = null;
		List<SpeciaMail> speciaMails = commDao.getSpeciaMails(10);
		long curTime = System.currentTimeMillis();
		boolean is = false;
		if (speciaMails != null && speciaMails.size() > 0)
		{
			for (SpeciaMail speciaMail : speciaMails)
			{
				if (speciaMail.isTrue(curTime))
				{
					is = true;
					break;
				}
			}
		}
		if (is)
		{
			chouCardLog = LotLog.createChouCardLog(playerId, 1, StringUtil.getDate(curTime));
			commDao.saveChouCardLog(chouCardLog);
			// 十连抽n次发奖励
			shiChouCardSpeciaMail(chouCardLog.getId(), curTime, speciaMails, playerId, name, level);
		}
	}
	
	/** 十连抽活动发奖励 **/
	public void shiChouCardSpeciaMail(int cid, long curTime, List<SpeciaMail> speciaMails, int id, String name, int level)
	{
		if (speciaMails != null && speciaMails.size() > 0)
		{
			for (int k = 0; k < speciaMails.size(); k++)
			{
				SpeciaMail speciaMail1 = speciaMails.get(k);
				if (speciaMail1.isTrue(curTime))
				{
					int curChouCardNums = commDao.getChouCardLogByDate(0, id, speciaMail1.getBeginTime(), speciaMail1.getEndTime());
					int allChouCardNums = commDao.getChouCardLogByDate(cid, id, speciaMail1.getBeginTime(), speciaMail1.getEndTime());
					if (curChouCardNums >= speciaMail1.getValue() && allChouCardNums < speciaMail1.getValue())
					{
						Mail mail = Mail.createMail(id, "GM", speciaMail1.getTitle(), speciaMail1.getContent(), speciaMail1.getReward1(), speciaMail1.getReward2(), speciaMail1.getReward3(), speciaMail1.getReward4(), speciaMail1.getReward5(), speciaMail1.getReward6(), speciaMail1.getGold(), speciaMail1.getCrystal(), speciaMail1.getRuneNum(), speciaMail1.getPower(), speciaMail1.getFriendNum(), speciaMail1.getDeleteTime());
						Cache.getInstance().sendMail(mail);
						playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + id + "|" + name + "|等级：" + level + "|获得十连抽" + speciaMail1.getValue() + "次,奖励邮件");
					}
				}
			}
		}
	}
	
//	/** pvp排名n~n发奖励 **/
//	public void pvpRankingSpeciaMail(int id, String name, int level, int rank)
//	{
//		List<SpeciaMail> speciaMails = commDao.getSpeciaMails(11);
//		if (speciaMails != null && speciaMails.size() > 0)
//		{
//			long curTime = System.currentTimeMillis();
//			for (int k = 0; k < speciaMails.size(); k++)
//			{
//				SpeciaMail speciaMail1 = speciaMails.get(k);
//				if (speciaMail1.isTrue(curTime))
//				{
//					if (rank >= speciaMail1.getValue() && rank <= speciaMail1.getValue2())
//					{
//						Mail mail = Mail.createMail(id, "GM", speciaMail1.getTitle(), speciaMail1.getContent(), speciaMail1.getReward1(), speciaMail1.getReward2(), speciaMail1.getReward3(), speciaMail1.getReward4(), speciaMail1.getReward5(), speciaMail1.getReward6(), speciaMail1.getGold(), speciaMail1.getCrystal(), speciaMail1.getRuneNum(), speciaMail1.getPower(), speciaMail1.getFriendNum(), speciaMail1.getDeleteTime());
//						Cache.getInstance().sendMail(mail);
//						playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + id + "|" + name + "|等级：" + level + "|获得pvp排名" + speciaMail1.getValue() + "~" + speciaMail1.getValue2() + "之间,奖励邮件");
//					}
//				}
//			}
//		}
//	}
//	
//	/** 解锁x合体技能发奖励 **/
//	public void blockUnitSkillDataSpeciaMail(int id, String name, int level, int usid)
//	{
//		List<SpeciaMail> speciaMails = commDao.getSpeciaMails(12);
//		if (speciaMails != null && speciaMails.size() > 0)
//		{
//			long curTime = System.currentTimeMillis();
//			for (int k = 0; k < speciaMails.size(); k++)
//			{
//				SpeciaMail speciaMail1 = speciaMails.get(k);
//				if (speciaMail1.isTrue(curTime))
//				{
//					if (usid == speciaMail1.getValue())
//					{
//						Mail mail = Mail.createMail(id, "GM", speciaMail1.getTitle(), speciaMail1.getContent(), speciaMail1.getReward1(), speciaMail1.getReward2(), speciaMail1.getReward3(), speciaMail1.getReward4(), speciaMail1.getReward5(), speciaMail1.getReward6(), speciaMail1.getGold(), speciaMail1.getCrystal(), speciaMail1.getRuneNum(), speciaMail1.getPower(), speciaMail1.getFriendNum(), speciaMail1.getDeleteTime());
//						Cache.getInstance().sendMail(mail);
//						playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + id + "|" + name + "|等级：" + level + "|获得解锁合体技" + speciaMail1.getValue() + ",奖励邮件");
//					}
//				}
//			}
//		}
//	}
}
