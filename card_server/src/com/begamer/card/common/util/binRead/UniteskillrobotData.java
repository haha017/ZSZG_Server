package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.List;

public class UniteskillrobotData implements PropertyReader
{
	//编号	
	public int id;
	//玩家名称
	public String name;
	//等级
	public int level;
	//卡组位置1
	public int position1;
	//卡组位置2
	public int position2;
	//卡组位置3
	public int position3;
	//卡组位置4
	public int position4;
	//卡组位置5
	public int position5;
	//卡组位置6
	public int position6;
	//携带合体技
	public int uniteskill;
	
	public int power;
	
	public int[] cards;
	
	private static List<UniteskillrobotData> data=new ArrayList<UniteskillrobotData>();
	
	@Override
	public void addData()
	{
		cards=new int[6];
		cards[0]=position1;
		cards[1]=position2;
		cards[2]=position3;
		cards[3]=position4;
		cards[4]=position5;
		cards[5]=position6;
		
		data.add(this);
	}

	@Override
	public void parse(String[] ss)
	{
	}

	@Override
	public void resetData()
	{
		data.clear();
	}

	public static List<UniteskillrobotData> getDatas()
	{
		return data;
	}
	
}
