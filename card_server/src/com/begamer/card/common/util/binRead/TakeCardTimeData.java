package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class TakeCardTimeData implements PropertyReader{
	/** 编号id **/
	public int id;
	/**
	 * 冷却时间 小时为单位 如果到达冷却时间是免费 如果没到冷却时间是c列钻石花费
	 **/
	public int time;
	
	/** 钻石花费 **/
	public int cast;
	
	/** 获得积分 **/
	public int getintegral;
	/** 积分 **/
	public int interracialcast;
	
	private static HashMap<Integer, TakeCardTimeData> data = new HashMap<Integer, TakeCardTimeData>();

	@Override
	public void addData()
	{
		data.put(id, this);
	}

	@Override
	public void parse(String[] ss)
	{
		
	}

	@Override
	public void resetData()
	{
		data.clear();
	}
	
	public static TakeCardTimeData getData(int id)
	{
		return data.get(id); 
	}
	
}
