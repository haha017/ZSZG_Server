package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GameBoxData implements PropertyReader
{
	public int id;
	public int keyid;
	public int boxstar;
	public int droptpye;
	public int goodstpye;
	public int itemid;
	public int number;
	public int cost;
	public int probability;
	public int showup;
	public int totaltime;
	public int totalnumber;
	
	private static HashMap<Integer, GameBoxData> data =new HashMap<Integer, GameBoxData>();
	@Override
	public void addData()
	{
		data.put(id, this);
	}

	@Override
	public void parse(String[] ss) {

	}

	@Override
	public void resetData()
	{
		data.clear();
	}

	public static GameBoxData getGameBoxData(int index)
	{
		return data.get(index);
	}
	/**类型和掉落类型**/
	public static List<GameBoxData> getGameBoxDatas(int t ,int dt ,int type)
	{
		List<GameBoxData> list =new ArrayList<GameBoxData>();
		for(GameBoxData gData :data.values())
		{
			if(type ==5)
			{
				if(gData.boxstar ==t && gData.droptpye==dt)
				{
					list.add(gData);
				}
			}
			else if(type ==6)
			{
				if(gData.keyid ==t && gData.droptpye==dt)
				{
					list.add(gData);
				}
			}
			
		}
		return list;
	}
	
	public static List<GameBoxData> getBoxDatas(int index,int type)
	{
		List<GameBoxData> list =new ArrayList<GameBoxData>();
		for(GameBoxData gData :data.values())
		{
			if(type ==5)
			{
				if(gData.boxstar==index)
				{
					list.add(gData);
				}
			}
			else if(type ==6)
			{
				if(gData.keyid==index)
				{
					list.add(gData);
				}
			}
		}
		return list;
	}
 }
