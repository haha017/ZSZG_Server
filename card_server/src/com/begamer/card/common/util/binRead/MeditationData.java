package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.begamer.card.common.util.StringUtil;

public class MeditationData implements PropertyReader{

	/** 编号id **/
	public int id;
	/** 需求次数 **/
	public int timeNumber;
	/**奖励类型 1.item**/	
	public int type;
	/**奖励内容**/
	public int itemID;
	/**随机指示 0.随机，其他代表下一个出现的任务的id**/
	public int random;
	/**
	 * 奖励数量1&奖励1触发几率,奖励数量2&奖励2触发几率
	 **/
	public String rewards;
	private static HashMap<Integer, MeditationData> data = new HashMap<Integer, MeditationData>();
	private static List<MeditationData> dataList = new ArrayList<MeditationData>();
	
	@Override
	public void addData()
	{
		data.put(id, this);
		dataList.add(this);
	}
	
	@Override
	public void parse(String[] ss)
	{
		int location = 0;
		id = StringUtil.getInt(ss[location]);
		timeNumber = StringUtil.getInt(ss[location + 1]);
		type = StringUtil.getInt(ss[location + 2]);
		itemID = StringUtil.getInt(ss[location + 3]);
		random = StringUtil.getInt(ss[location + 4]);
		int length = (ss.length - 5) /4;
		for (int i = 0; i < length; i++)
		{
			location = 5 + i * 4;
			int num = StringUtil.getInt(ss[location]);
			String pro = StringUtil.getString(ss[location + 1]);
			int num2 = StringUtil.getInt(ss[location + 2]);
			int pro2 = StringUtil.getInt(ss[location + 3]);
			rewards = num + "&" + pro + "&" + num2 + "&" + pro2;
		}
		addData();
	}
	
	@Override
	public void resetData()
	{
		data.clear();
		dataList.clear();
	}
	
	public static MeditationData getData(int id)
	{
		return data.get(id);
	}
	
	public static List<MeditationData> getDatas()
	{
		return dataList;
	}
	
	public static MeditationData getMeditationDataByRandom(int random)
	{
		for (MeditationData md : dataList)
		{
			if (md.random == random)
			{
				return md;
			}
		}
		return null;
	}
}
