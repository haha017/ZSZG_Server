package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.begamer.card.common.util.StringUtil;

public class IntegralFirstData implements PropertyReader {
	
	/** 编号id **/
	public int id;
	/** 积分名次 **/
	public int pank;
	/**奖励邮件标题**/	
	public String mailtab;
	/**邮件内容**/
	public String mailtext;
	/**积分值**/
	public int integralvalue;


	/**
	 * 奖励类型 格式：type-item(id,num) 1.items,
	 * 2.equip,3.card,4.skill,5.passiveskill,6.金币,7.人物经验值,8.水晶,9.符文值,10.体力,11.友情值,12.金罡
	 * 心
	 **/
	public String[] rewards;
	private static HashMap<Integer, IntegralFirstData> data = new HashMap<Integer, IntegralFirstData>();
	private static List<IntegralFirstData> dataList = new ArrayList<IntegralFirstData>();
	
	@Override
	public void addData()
	{
		data.put(pank, this);
		dataList.add(this);
	}
	
	@Override
	public void parse(String[] ss)
	{
		int location = 0;
		id = StringUtil.getInt(ss[location]);
		pank = StringUtil.getInt(ss[location + 1]);
		mailtab = ss[location + 2];
		mailtext = ss[location + 3];
		integralvalue = StringUtil.getInt(ss[location + 4]);
		int length = (ss.length - 5) / 2;
		rewards = new String[5];
		for (int i = 0; i < length; i++)
		{
			location = 5 + i * 2;
			int type = StringUtil.getInt(ss[location]);
			String info = StringUtil.getString(ss[location + 1]);
			String str = "";
			if (type > 0 && type <= 5)
			{
				String[] temp = info.split(",");
				str = type + "&" + temp[0] + "&" + temp[1];
			}
			else
			{
				str = type + "&" + info;
			}
			rewards[i] = str;
		}
		addData();
	}
	
	@Override
	public void resetData()
	{
		data.clear();
		dataList.clear();
	}
	
	public static IntegralFirstData getData(int id)
	{
		return data.get(id);
	}
	
	public static List<IntegralFirstData> getDatas()
	{
		return dataList;
	}
}
