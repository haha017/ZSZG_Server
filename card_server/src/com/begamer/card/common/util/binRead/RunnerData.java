package com.begamer.card.common.util.binRead;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import com.begamer.card.common.util.StringUtil;

public class RunnerData implements PropertyReader
{
	public int id;
	/**活动状态**/
	public int state;
	/**活动类型**/
	public int type;
	/**名字**/
	public String text;
	/**档位图标**/
	public String icon;
	/**达成条件**/
	public int condition;
	/**抽奖次数**/
	public int time;
	/**转盘物品**/
	public List<String> item_pro;
	/**循环次数**/
	public int circulationtime;
	
	private static HashMap<Integer, RunnerData> data =new HashMap<Integer, RunnerData>();
	
	@Override
	public void addData()
	{
		data.put(id, this);
	}

	@Override
	public void parse(String[] ss)
	{
		int location=0;
		id =StringUtil.getInt(ss[location]);
		state =StringUtil.getInt(ss[location+1]);
		type =StringUtil.getInt(ss[location+2]);
		text =StringUtil.getString(ss[location+3]);
		icon =StringUtil.getString(ss[location+4]);
		condition =StringUtil.getInt(ss[location+5]);
		time =StringUtil.getInt(ss[location+6]);
		item_pro =new ArrayList<String>();
		for(int i=0;i<8;i++)
		{
			location =7+i*5;
			int iType =StringUtil.getInt(ss[location]);
			if(iType !=0)
			{
				String item =StringUtil.getString(ss[location+1]);
				int pro1 =StringUtil.getInt(ss[location+2]);
				int pro2 =StringUtil.getInt(ss[location+3]);
				int radio =StringUtil.getInt(ss[location+4]);
				String item_info =iType+"-"+item+"-"+pro1+"-"+pro2+"-"+radio;
				item_pro.add(item_info);
			}
			else
			{
				continue;
			}
		}
		location =7+8*4;
		circulationtime =StringUtil.getInt(ss[location]);
		addData();
	}

	@Override
	public void resetData()
	{
		data.clear();
	}

	/**根据id获取一个data**/
	public static RunnerData getRunnerData(int index)
	{
		return data.get(index);
	}
	/**获取同一个类型的datas***/
	public static List<RunnerData> getRunnerDatas(int t)
	{
		List<RunnerData> list =new ArrayList<RunnerData>();
		for(RunnerData rData:data.values())
		{
			if(rData.type==t)
			{
				list.add(rData);
			}
		}
		return list;
	}
	
	/**根据激活状态获取datas**/
	public static List<RunnerData> getDatasByState(int t)
	{
		List<RunnerData> list =new ArrayList<RunnerData>();
		for(RunnerData rData:data.values())
		{
			if(rData.state==t)
			{
				list.add(rData);
			}
		}
		return list;
	}
	
	/**根据类型和达成条件获取一个data**/
	public static RunnerData getRunner(int t,int num)
	{
		for(RunnerData rData :data.values())
		{
			if(rData.type==t && rData.condition==num && rData.state==1)
			{
				return rData;
			}
		}
		return null;
	}
	
	public static List<RunnerData> getDatasByType(int t)
	{
		List<RunnerData> list =new ArrayList<RunnerData>();
		for(RunnerData rData :data.values())
		{
			if(rData.type==t && rData.state==1)
			{
				list.add(rData);
			}
		}
		return list;
	}
}
