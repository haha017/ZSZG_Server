package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.begamer.card.common.util.StringUtil;

public class RacePowerData implements PropertyReader
{
	public int id;
	/**人数**/
	public int number;
	/**属性类型-数值**/
	public List<String> attris;

	private static HashMap<Integer, RacePowerData> data=new HashMap<Integer, RacePowerData>();
	
	@Override
	public void addData()
	{
		data.put(number, this);
	}

	@Override
	public void parse(String[] ss)
	{
		id=StringUtil.getInt(ss[0]);
		number=StringUtil.getInt(ss[1]);
		attris=new ArrayList<String>();
		int length=(ss.length-2)/2;
		for(int k=0;k<length;k++)
		{
			int location=k*2+2;
			int type=StringUtil.getInt(ss[location]);
			int num=StringUtil.getInt(ss[location+1]);
			if(type>0 && num>0)
			{
				attris.add(type+"-"+num);
			}
		}
		addData();
	}

	@Override
	public void resetData()
	{
		data.clear();
	}

	public static RacePowerData getData(int number)
	{
		return data.get(number);
	}
	
}
