package com.begamer.card.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.begamer.card.common.Constant;
import com.begamer.card.common.util.GmSpecialMailUtil;
import com.begamer.card.common.util.Random;
import com.begamer.card.common.util.Statics;
import com.begamer.card.common.util.StringUtil;
import com.begamer.card.common.util.binRead.AchievementData;
import com.begamer.card.common.util.binRead.CardData;
import com.begamer.card.common.util.binRead.EquipData;
import com.begamer.card.common.util.binRead.ItemsData;
import com.begamer.card.common.util.binRead.MissionData;
import com.begamer.card.common.util.binRead.PassiveSkillData;
import com.begamer.card.common.util.binRead.SkillData;
import com.begamer.card.common.util.binRead.UnitSkillData;
import com.begamer.card.json.CardJson;
import com.begamer.card.json.command2.BattleLogResultJson;
import com.begamer.card.json.command2.BattleResultJson;
import com.begamer.card.json.command2.BuyPowerOrGoldJson;
import com.begamer.card.json.command2.BuyPowerOrGoldResultJson;
import com.begamer.card.json.command2.EventBattleLogResultJson;
import com.begamer.card.json.command2.EventBattleResultJson;
import com.begamer.card.json.command2.EventResultJson;
import com.begamer.card.json.command2.MazeBattleLogResultJson;
import com.begamer.card.json.command2.MazeBattleResultJson;
import com.begamer.card.json.command2.MazeResultJson;
import com.begamer.card.json.command2.PkBattleLogResultJson;
import com.begamer.card.json.command2.PkBattleResultJson;
import com.begamer.card.log.PlayerLogger;
import com.begamer.card.model.pojo.Card;
import com.begamer.card.model.pojo.CardGroup;
import com.begamer.card.model.pojo.Equip;
import com.begamer.card.model.pojo.Formation;
import com.begamer.card.model.pojo.Friend;
import com.begamer.card.model.pojo.Item;
import com.begamer.card.model.pojo.Mail;
import com.begamer.card.model.pojo.Maze;
import com.begamer.card.model.pojo.PassiveSkill;
import com.begamer.card.model.pojo.Player;
import com.begamer.card.model.pojo.Skill;

public class PlayerInfo {
	// 客户端
	private HttpSession session;
	// 玩家信息
	public Player player;
	private CopyOnWriteArrayList<Card> cards;
	private CopyOnWriteArrayList<Skill> skills;
	private CopyOnWriteArrayList<PassiveSkill> passiveSkills;
	private CopyOnWriteArrayList<Equip> equips;
	private CopyOnWriteArrayList<Item> items;
	/** 存库的卡组信息 **/
	private Formation formation;
	/** 用于玩家展示的卡组信息 **/
	private CardGroup cg;
	/** 好友 **/
	private List<Friend> friends;
	/** 邮件 **/
	private List<Mail> mails;
	/** 好友临时信息 **/
	public long lastRefreshTime;
	/** 元素格式:friendId **/
	public List<Integer> lastRefreshList;
	public int lastSearch;
	/** 战斗临时信息 **/
	public int bNum;// 战斗场次,用于校验战斗,客户端可能会重复发送战斗结果
	public BattleLogResultJson blrj;// 战斗后服务器发给客户端的json
	public MazeBattleLogResultJson mblrj;// 迷宫战斗后服务器发给客户端的json
	public BattleResultJson brj;// 初始战斗时发给客户端的json,用于校验战斗
	public boolean needAddHelperAsFriend;// 是否需要添加支援玩家为好友
	public int helperPlayerId;// 支援玩家Id
	public String helperPlayerName;// 支援玩家名字
	public MazeBattleResultJson mbrj;// 初始迷宫战斗时发给客户端的json，用于下发初始数据，校验战斗
	public PkBattleResultJson pbrj;// 初始pk战斗时发给客户端的json，用于下发初始数据，校验战斗
	public PkBattleLogResultJson pblrj;// pk战斗后服务器发给客户端的json
	public EventBattleResultJson ebrj;// 副本战斗初始数据，服务器下发给客户端
	public EventBattleLogResultJson eblrj;// 副本战斗结束后，服务器发给客户端json
	public EventResultJson erj;
	public BuyPowerOrGoldJson bJson;// 购买时客户端->服务器
	public BuyPowerOrGoldResultJson buyJson;// 购买时服务器->客户端
	public MazeResultJson mrj;
	
	private Logger playerLogger = PlayerLogger.logger;
	
	/** 冷却时间 **/
	public long pkcdtime;// pk冷却时间
	public long mazecdtime;// 迷宫冷却时间
	public long eventcdtime1;// 金币副本冷却时间
	public long eventcdtime2;// 经验副本
	public long eventcdtime3;// 特殊副本
	// 解锁的新的合体技id&id
	public String newunitskills;
	
	/** 迷宫进度 **/
	public CopyOnWriteArrayList<Maze> mazes = new CopyOnWriteArrayList<Maze>();
	
	public PlayerInfo(Player player, List<Card> cards, List<Skill> skills, List<PassiveSkill> passiveSkills, List<Equip> equips, List<Item> items, Formation formation, List<Friend> friends, List<Mail> mails, List<Maze> mazes)
	{
		this.player = player;
		this.cards = new CopyOnWriteArrayList<Card>();
		this.cards.addAll(cards);
		this.skills = new CopyOnWriteArrayList<Skill>();
		this.skills.addAll(skills);
		this.passiveSkills = new CopyOnWriteArrayList<PassiveSkill>();
		this.passiveSkills.addAll(passiveSkills);
		this.equips = new CopyOnWriteArrayList<Equip>();
		this.equips.addAll(equips);
		this.items = new CopyOnWriteArrayList<Item>();
		this.items.addAll(items);
		this.formation = formation;
		this.friends = friends;
		this.mails = mails;
		this.mazes = new CopyOnWriteArrayList<Maze>();
		this.mazes.addAll(mazes);
		init();
	}
	
	public HttpSession getSession()
	{
		return session;
	}
	
	public void setSession(HttpSession session)
	{
		this.session = session;
	}
	
	public Formation getFormation()
	{
		return formation;
	}
	
	public CardGroup getCardGroup()
	{
		return cg;
	}
	
	public boolean isValid()
	{
		try
		{
			return (session != null && (System.currentTimeMillis() - session.getLastAccessedTime()) / 1000 < session.getMaxInactiveInterval());
		}
		catch (Exception e)
		{
			return false;
		}
	}
	
	public boolean willOnline()
	{
		try
		{
			return (session != null && (System.currentTimeMillis() - session.getLastAccessedTime()) / 1000 < session.getMaxInactiveInterval() - 2 * 60);
		}
		catch (Exception e)
		{
			return false;
		}
	}
	
	public boolean addItem(int itemId, int num)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + player.getId() + "|" + player.getName() + "|等级：" + player.getLevel() + "|获得材料|" + itemId + "|" + num);
		if (num <= 0 || ItemsData.getItemsData(itemId) == null)
		{
			return false;
		}
		// 处理叠加
		for (Item item : getItems())
		{
			if (item.getItemId() == itemId)
			{
				ItemsData itemsData = ItemsData.getItemsData(itemId);
				if (item.getPile() + num > itemsData.pile)
				{
					num = item.getPile() + num - itemsData.pile;
					item.setPile(itemsData.pile);
				}
				else
				{
					item.setPile(item.getPile() + num);
					num = 0;
					break;
				}
			}
		}
		while (num > 0)
		{
			ItemsData iData = ItemsData.getItemsData(itemId);
			if (num > iData.pile)
			{
				Item i = Item.createItem(player.getId(), itemId, iData.pile);
				num = num - iData.pile;
				items.add(i);
			}
			else
			{
				Item i = Item.createItem(player.getId(), itemId, num);
				num = 0;
				items.add(i);
			}
		}
		return true;
	}
	
	public boolean addEquip(int equipId)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + player.getId() + "|" + player.getName() + "|等级：" + player.getLevel() + "|获得装备|" + equipId);
		if (EquipData.getData(equipId) == null)
		{
			return false;
		}
		Equip e = Equip.createEquip(player.getId(), equipId);
		equips.add(e);
		// 更新成就 x星卡x张
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		map = getEquipNumMap();
		List<AchievementData> ads = AchievementData.getAchievementByType(9);
		for (AchievementData ad : ads)
		{
			String[] temp = ad.request.split(",");
			if (temp[0] != null && temp[0].length() > 0)
			{
				if (map.get(StringUtil.getInt(temp[0])) == null)
				{
					player.updateAchieve(9, temp[0] + "," + 0);
				}
				else
				{
					player.updateAchieve(9, temp[0] + "," + map.get(StringUtil.getInt(temp[0])));
				}
			}
		}
		return true;
	}
	
	public Card addCard(int cardId, int level)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + player.getId() + "|" + player.getName() + "|等级：" + player.getLevel() + "|获得卡牌|" + cardId);
		CardData cData = CardData.getData(cardId);
		if (cData == null)
		{
			return null;
		}
		Card c = Card.createCard(player.getId(), cardId, level);
		cards.add(c);
		// 更新成就 x星卡x张
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		map = getCardNumMap();
		List<AchievementData> ads = AchievementData.getAchievementByType(6);
		for (AchievementData ad : ads)
		{
			String[] temp = ad.request.split(",");
			if (temp[0] != null && temp[0].length() > 0)
			{
				if (map.get(StringUtil.getInt(temp[0])) == null)
				{
					player.updateAchieve(6, temp[0] + "," + 0);
				}
				else
				{
					player.updateAchieve(6, temp[0] + "," + map.get(StringUtil.getInt(temp[0])));
				}
			}
		}
		// 更新成就 x张n1卡n2卡
		List<AchievementData> ads2 = AchievementData.getAchievementByType(5);
		HashMap<Integer, Card> map1 = getCardMap();
		for (AchievementData ad : ads2)
		{
			String[] temp = ad.request.split(",");
			if (cards.size() > (temp.length - 1) && temp.length > 1)
			{
				int n = 0;
				String con = "";
				for (int i = 1; i < temp.length; i++)
				{
					if (map1.containsKey(StringUtil.getInt(temp[i])))
					{
						
						n++;
						if (con == null || con.length() == 0)
						{
							con = temp[i];
						}
						else
						{
							con = con + "," + temp[i];
						}
					}
				}
				player.updateAchieve(5, n + "," + con);
			}
		}
		return c;
	}
	
	public boolean addSkill(int skillId, int lv)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + player.getId() + "|" + player.getName() + "|等级：" + player.getLevel() + "|获得主动技能|" + skillId);
		if (SkillData.getData(skillId) == null)
		{
			return false;
		}
		Skill skill = Skill.createSkill(player.getId(), skillId, lv);
		skills.add(skill);
		// 更新成就 x星卡x张
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		map = getSkillNumMap();
		List<AchievementData> ads = AchievementData.getAchievementByType(7);
		for (AchievementData ad : ads)
		{
			String[] temp = ad.request.split(",");
			if (temp[0] != null && temp[0].length() > 0)
			{
				if (map.get(StringUtil.getInt(temp[0])) == null)
				{
					player.updateAchieve(7, temp[0] + "," + 0);
				}
				else
				{
					player.updateAchieve(7, temp[0] + "," + map.get(StringUtil.getInt(temp[0])));
				}
			}
		}
		return true;
	}
	
	public boolean addPassiveSkill(int pSkillId)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + player.getId() + "|" + player.getName() + "|等级：" + player.getLevel() + "|获得被动技能|" + pSkillId);
		if (PassiveSkillData.getData(pSkillId) == null)
		{
			return false;
		}
		PassiveSkill pskill = PassiveSkill.createPassiveSkill(player.getId(), pSkillId);
		passiveSkills.add(pskill);
		
		// 更新成就 x星卡x张
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		map = getPskillNumMap();
		List<AchievementData> ads = AchievementData.getAchievementByType(8);
		for (AchievementData ad : ads)
		{
			String[] temp = ad.request.split(",");
			if (temp[0] != null && temp[0].length() > 0)
			{
				if (map.get(StringUtil.getInt(temp[0])) == null)
				{
					player.updateAchieve(8, temp[0] + "," + 0);
				}
				else
				{
					player.updateAchieve(8, temp[0] + "," + map.get(StringUtil.getInt(temp[0])));
				}
			}
		}
		return true;
	}
	
	public List<Friend> getFriends()
	{
		List<Friend> fs = new ArrayList<Friend>();
		for (int k = 0; k < friends.size(); k++)
		{
			Friend f = friends.get(k);
			if (f.getState() != 1)
			{
				fs.add(f);
			}
		}
		return fs;
	}
	
	public Friend getFriend(int friendId)
	{
		for (int k = 0; k < friends.size(); k++)
		{
			Friend f = friends.get(k);
			if (f.getState() != 1 && f.getFriendId() == friendId)
			{
				return f;
			}
		}
		return null;
	}
	
	public boolean isFriend(int friendId)
	{
		List<Friend> list = getFriends();
		for (Friend f : list)
		{
			if (f.getFriendId() == friendId)
			{
				return true;
			}
		}
		return false;
	}
	
	public void addFriend(Friend f)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + player.getId() + "|" + player.getName() + "|等级：" + player.getLevel() + "|添加好友|" + f.getFriendId());
		friends.add(f);
		// 更新成就
		player.updateAchieve(10, friends.size() + "");
	}
	
	public void addMail(Mail m)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + player.getId() + "|" + player.getName() + "|等级：" + player.getLevel() + "|添加邮件|" + m.getTitle());
		mails.add(0, m);
	}
	
	public List<Mail> getAllMails()
	{
		return mails;
	}
	
	public List<Mail> getMails()
	{
		List<Mail> result = new ArrayList<Mail>();
		for (int i = 0; i < mails.size(); i++)
		{
			Mail m = mails.get(i);
			if (m.getState() == 1)
			{
				continue;
			}
			result.add(m);
		}
		return result;
	}
	
	public void getMailAttach(Mail mail)
	{
		if (mail == null)
		{
			return;
		}
		String reward1 = mail.getReward1();
		String reward2 = mail.getReward2();
		String reward3 = mail.getReward3();
		String reward4 = mail.getReward4();
		String reward5 = mail.getReward5();
		String reward6 = mail.getReward6();
		int gold = mail.getGold();
		int crystal = mail.getCrystal();
		int runeNum = mail.getRuneNum();
		int honor = mail.getHonor();
		int power = mail.getPower();
		int friendNum = mail.getFriendNum();
		int diamond = mail.getDiamond();
		
		List<Integer> cardIds = new ArrayList<Integer>();
		if (reward1 != null && !"".equals(reward1))
		{
			String[] ss = reward1.split("&");
			int type = StringUtil.getInt(ss[0]);
			int rewardId = StringUtil.getInt(ss[1]);
			int number = StringUtil.getInt(ss[2]);
			if (type > 0 && rewardId > 0 && number > 0)
			{
				if (type == 3)
				{
					cardIds.add(rewardId);
				}
				Statics.getReward(type, rewardId, number, this);
			}
		}
		if (reward2 != null && !"".equals(reward2))
		{
			String[] ss = reward2.split("&");
			int type = StringUtil.getInt(ss[0]);
			int rewardId = StringUtil.getInt(ss[1]);
			int number = StringUtil.getInt(ss[2]);
			if (type > 0 && rewardId > 0 && number > 0)
			{
				if (type == 3)
				{
					cardIds.add(rewardId);
				}
				Statics.getReward(type, rewardId, number, this);
			}
		}
		if (reward3 != null && !"".equals(reward3))
		{
			String[] ss = reward3.split("&");
			int type = StringUtil.getInt(ss[0]);
			int rewardId = StringUtil.getInt(ss[1]);
			int number = StringUtil.getInt(ss[2]);
			if (type > 0 && rewardId > 0 && number > 0)
			{
				if (type == 3)
				{
					cardIds.add(rewardId);
				}
				Statics.getReward(type, rewardId, number, this);
			}
		}
		if (reward4 != null && !"".equals(reward4))
		{
			String[] ss = reward4.split("&");
			int type = StringUtil.getInt(ss[0]);
			int rewardId = StringUtil.getInt(ss[1]);
			int number = StringUtil.getInt(ss[2]);
			if (type > 0 && rewardId > 0 && number > 0)
			{
				if (type == 3)
				{
					cardIds.add(rewardId);
				}
				Statics.getReward(type, rewardId, number, this);
			}
		}
		if (reward5 != null && !"".equals(reward5))
		{
			String[] ss = reward5.split("&");
			int type = StringUtil.getInt(ss[0]);
			int rewardId = StringUtil.getInt(ss[1]);
			int number = StringUtil.getInt(ss[2]);
			if (type > 0 && rewardId > 0 && number > 0)
			{
				if (type == 3)
				{
					cardIds.add(rewardId);
				}
				Statics.getReward(type, rewardId, number, this);
			}
		}
		if (reward6 != null && !"".equals(reward6))
		{
			String[] ss = reward6.split("&");
			int type = StringUtil.getInt(ss[0]);
			int rewardId = StringUtil.getInt(ss[1]);
			int number = StringUtil.getInt(ss[2]);
			if (type > 0 && rewardId > 0 && number > 0)
			{
				if (type == 3)
				{
					cardIds.add(rewardId);
				}
				Statics.getReward(type, rewardId, number, this);
			}
		}
		if (gold > 0)
		{
			player.addGold(gold);
		}
		if (crystal > 0)
		{
			player.addCrystal(crystal);
		}
		if (runeNum > 0)
		{
			player.addRuneNum(runeNum);
		}
		if (honor > 0)
		{
			player.addPvpHonor(honor);
		}
		if (power > 0)
		{
			player.addPower(power, false);
		}
		if (friendNum > 0)
		{
			player.addFriendValue(friendNum);
		}
		if (cardIds.size() > 0)// 更新解锁合体技
		{
			getNewUnitSkill(cardIds);
		}
		if (diamond > 0)
		{
			player.addDiamond(diamond);
		}
	}
	
	/**
	 * 删除好友 lt@2014-2-28 上午09:49:16
	 * 
	 * @param index
	 */
	public void removeFriend(int index)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + player.getId() + "|" + player.getName() + "|等级：" + player.getLevel() + "|删除好友|");
		Friend fid = getFriends().get(index);
		fid.setState(1);
	}
	
	public List<Integer> getRefreshFriends(boolean isOpen)
	{
		if (isOpen && lastRefreshList != null && lastRefreshList.size() > 0)
		{
			for (int k = lastRefreshList.size() - 1; k >= 0; k--)
			{
				int friendId = lastRefreshList.get(k);
				for (Friend f : getFriends())
				{
					if (friendId == f.getFriendId())
					{
						lastRefreshList.remove(k);
						break;
					}
				}
			}
			return lastRefreshList;
		}
		else
		{
			List<Integer> result = new ArrayList<Integer>();
			List<Friend> applys = Cache.getInstance().getMyApplys(player.getId());
			for (int playerId : Cache.getInstance().getTempPlayerKeys())
			{
				boolean canAdd = true;
				if (playerId == player.getId())
				{
					canAdd = false;
				}
				if (canAdd)
				{
					for (Friend f : friends)
					{
						if ((f.getState() != 1 && f.getFriendId() == playerId) || playerId == player.getId())
						{
							canAdd = false;
							break;
						}
					}
				}
				if (canAdd)
				{
					for (Friend f : applys)
					{
						if (f.getFriendId() == playerId)
						{
							canAdd = false;
							break;
						}
					}
				}
				if (canAdd)
				{
					result.add(playerId);
				}
			}
			if (result.size() > Constant.MaxFefreshUnapplyNum)
			{
				List<Integer> temp = new ArrayList<Integer>();
				for (int k = 0; k < Constant.MaxFefreshUnapplyNum; k++)
				{
					int random = Random.getNumber(result.size());
					temp.add(result.remove(random));
				}
				result = temp;
			}
			if (!isOpen)
			{
				lastRefreshTime = System.currentTimeMillis();
			}
			lastRefreshList = result;
			return lastRefreshList;
		}
	}
	
	public List<Card> getCards()
	{
		List<Card> list = new ArrayList<Card>();
		for (int i = 0; i < cards.size(); i++)
		{
			if (cards.get(i).getSell() == 0)
			{
				list.add(cards.get(i));
			}
		}
		return list;
	}
	
	public void removeItem(int itemId, int num)
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + player.getId() + "|" + player.getName() + "|等级：" + player.getLevel() + "|消耗材料|" + itemId);
		if (getTargetItemsNum(itemId) < num)
		{
			return;
		}
		List<Item> temp = getTargetItems(itemId);
		while (num > 0)
		{
			Item itemTemp = temp.get(temp.size() - 1);
			if (itemTemp.getPile() > num)
			{
				itemTemp.setPile(itemTemp.getPile() - num);
				num = 0;
			}
			else
			{
				temp.remove(itemTemp);
				itemTemp.setSell(1);
				num = num - itemTemp.getPile();
			}
		}
	}
	
	public int getTargetItemsNum(int itemId)
	{
		int result = 0;
		for (Item item : getTargetItems(itemId))
		{
			result += item.getPile();
		}
		return result;
	}
	
	/**
	 * 获取指定材料 lt@2014-4-8 下午09:50:32
	 * 
	 * @param itemId
	 * @return
	 */
	public List<Item> getTargetItems(int itemId)
	{
		List<Item> result = new ArrayList<Item>();
		for (Item item : getItems())
		{
			if (item.getItemId() == itemId)
			{
				result.add(item);
			}
		}
		return result;
	}
	
	public List<Skill> getSkills()
	{
		List<Skill> list = new ArrayList<Skill>();
		for (int i = 0; i < skills.size(); i++)
		{
			if (skills.get(i).getSell() == 0)
			{
				list.add(skills.get(i));
			}
		}
		return list;
	}
	
	/** 获取某个物品的个数 **/
	public int getTargetNum(int type, int equipId)
	{
		int m = 0;
		switch (type)
		{
			case 2:// equip
				for (int i = 0; i < getEquips().size(); i++)
				{
					if (getEquips().get(i).getEquipId() == equipId)
					{
						m++;
					}
				}
				break;
			case 3:// card
				for (int i = 0; i < getCards().size(); i++)
				{
					if (getCards().get(i).getCardId() == equipId)
					{
						m++;
					}
				}
				break;
			case 4:// skill
				for (int i = 0; i < getSkills().size(); i++)
				{
					if (getSkills().get(i).getSkillId() == equipId)
					{
						m++;
					}
				}
				break;
			case 5:// ps
				for (int i = 0; i < getPassiveSkillls().size(); i++)
				{
					if (getPassiveSkillls().get(i).getPassiveSkillId() == equipId)
					{
						m++;
					}
				}
				break;
		}
		
		return m;
	}
	
	/** 保存使用 **/
	public List<Friend> getAllFriends()
	{
		return friends;
	}
	
	public List<Card> getAllCards()
	{
		return cards;
	}
	
	public List<Skill> getAllSkills()
	{
		return skills;
	}
	
	public List<PassiveSkill> getAllPassiveSkills()
	{
		return passiveSkills;
	}
	
	public List<Equip> getAllEquips()
	{
		return equips;
	}
	
	public List<Item> getAllItems()
	{
		return items;
	}
	
	public List<Item> getItems()
	{
		List<Item> list = new ArrayList<Item>();
		for (int i = 0; i < items.size(); i++)
		{
			if (items.get(i).getSell() == 0)
			{
				list.add(items.get(i));
			}
		}
		return list;
	}
	
	public List<PassiveSkill> getPassiveSkillls()
	{
		List<PassiveSkill> list = new ArrayList<PassiveSkill>();
		for (int i = 0; i < passiveSkills.size(); i++)
		{
			if (passiveSkills.get(i).getSell() == 0)
			{
				list.add(passiveSkills.get(i));
			}
		}
		return list;
	}
	
	public List<Equip> getEquips()
	{
		List<Equip> list = new ArrayList<Equip>();
		for (int i = 0; i < equips.size(); i++)
		{
			if (equips.get(i).getSell() == 0)
			{
				list.add(equips.get(i));
			}
		}
		return list;
	}
	
	/** bonus1奖励map **/
	public HashMap<Integer, Integer> getBonusMap1()
	{
		HashMap<Integer, Integer> bMap = new HashMap<Integer, Integer>();
		String[] bonus1 = player.getBouns1().split(",");
		for (int k = 0; k < bonus1.length; k++)
		{
			if (bonus1[k] != null && bonus1[k].length() > 0)
			{
				int bonus = StringUtil.getInt(bonus1[k]);
				bMap.put(bonus, bonus);
			}
		}
		return bMap;
	}
	
	/** bonus2Map **/
	public HashMap<Integer, Integer> getBonusMap2()
	{
		HashMap<Integer, Integer> bMap = new HashMap<Integer, Integer>();
		String[] bonus2 = player.getBouns2().split(",");
		for (int k = 0; k < bonus2.length; k++)
		{
			if (bonus2[k] != null && bonus2[k].length() > 0)
			{
				int bonus = StringUtil.getInt(bonus2[k]);
				bMap.put(bonus, bonus);
			}
		}
		return bMap;
	}
	
	/** 副本进入次数1 **/
	public HashMap<Integer, String> getFBnumMap1()
	{
		HashMap<Integer, String> fBMap = new HashMap<Integer, String>();
		if (player.getFBnum1() != null && player.getFBnum1().length() > 0)
		{
			String[] str = player.getFBnum1().split(",");
			for (int k = 0; k < str.length; k++)
			{
				if (str[k] != null && str[k].length() > 0)
				{
					String[] temp = str[k].split("-");
					int fbid = StringUtil.getInt(temp[0]);
					fBMap.put(fbid, str[k]);
				}
			}
		}
		return fBMap;
	}
	
	public HashMap<Integer, String> getFBnumMap2()
	{
		HashMap<Integer, String> fBMap = new HashMap<Integer, String>();
		if (player.getFBnum2() != null && player.getFBnum2().length() > 0)
		{
			String[] str = player.getFBnum2().split(",");
			for (int k = 0; k < str.length; k++)
			{
				if (str[k] != null && str[k].length() > 0)
				{
					String[] temp = str[k].split("-");
					int fbid = StringUtil.getInt(temp[0]);
					fBMap.put(fbid, str[k]);
				}
			}
		}
		return fBMap;
	}
	
	private List<Integer> getRandomResult(List<Integer> list)
	{
		List<Integer> result = new ArrayList<Integer>();
		int num = 0;
		while (list.size() > 0 && num < 1000)
		{
			num++;
			int roll = Random.getNumber(list.size());
			int element = list.remove(roll);
			result.add(element);
		}
		return result;
	}
	
	/** helperId-state-unitId,state:0好友,3陌生人 **/
	public List<String> getHelpers() throws Exception
	{
		List<String> result = new ArrayList<String>();
		
		List<Integer> friendIds = new ArrayList<Integer>();
		for (Friend f : friends)
		{
			if (!StringUtil.getDate(System.currentTimeMillis()).equals(f.getLastHelpDay()))
			{
				if (Cache.getInstance().getTempUnitId(f.getFriendId()) != 0)
				{
					friendIds.add(f.getFriendId());
				}
			}
		}
		// 打乱顺序
		friendIds = getRandomResult(friendIds);
		// 5个好友
		int friendNum = 5;
		for (int k = 0; k < friendNum && k < friendIds.size(); k++)
		{
			int playerId = friendIds.get(k);
			result.add(playerId + "-" + 0 + "-" + Cache.getInstance().getTempUnitId(playerId));
		}
		// 5个陌生人
		int otherNum = 10 - result.size();
		List<Integer> playerIds = new ArrayList<Integer>();
		playerIds.addAll(Cache.getInstance().getTempPlayerKeys());
		List<Integer> idTemp = new ArrayList<Integer>();
		for (int playerId : playerIds)
		{
			if (Cache.getInstance().getTempUnitId(playerId) == 0 || playerId == player.getId() || isFriend(playerId))
			{
				continue;
			}
			idTemp.add(playerId);
		}
		// 打乱顺序
		idTemp = getRandomResult(idTemp);
		// 80%概率刷出自身等级<=x<=自身等级+5,20%刷出>自身等级+5
		int rollPro = Random.getNumber(100);
		List<Integer> idTemp2 = new ArrayList<Integer>();
		idTemp2.addAll(idTemp);
		for (int k = idTemp.size() - 1; k >= 0; k--)
		{
			int playerId = idTemp.get(k);
			int tempLevel = Cache.getInstance().getTempLevel(playerId);
			if (rollPro < 80)
			{
				if (tempLevel < player.getLevel() || tempLevel > player.getLevel() + 5)
				{
					idTemp.remove(k);
				}
			}
			else
			{
				if (tempLevel <= player.getLevel() + 5)
				{
					idTemp.remove(k);
				}
			}
		}
		// 如果没有则向下刷,80%自身等级-5<=x<=自身等级,20%x<自身等级-5
		if (idTemp.size() == 0)
		{
			List<Integer> idTemp3 = new ArrayList<Integer>();
			idTemp3.addAll(idTemp2);
			
			rollPro = Random.getNumber(100);
			for (int k = idTemp2.size() - 1; k >= 0; k--)
			{
				int playerId = idTemp2.get(k);
				int tempLevel = Cache.getInstance().getTempLevel(playerId);
				if (rollPro < 80)
				{
					if (tempLevel < player.getLevel() - 5 || tempLevel > player.getLevel())
					{
						idTemp2.remove(k);
					}
				}
				else
				{
					if (tempLevel >= player.getLevel() - 5)
					{
						idTemp2.remove(k);
					}
				}
			}
			idTemp.addAll(idTemp2);
			// 如果还没有刷到,则刷新x<自身等级
			if (idTemp.size() == 0)
			{
				for (int playerId : idTemp3)
				{
					int tempLevel = Cache.getInstance().getTempLevel(playerId);
					if (tempLevel <= player.getLevel())
					{
						idTemp.add(playerId);
					}
				}
			}
		}
		for (int k = 0; k < otherNum && k < idTemp.size(); k++)
		{
			int playerId = idTemp.get(k);
			result.add(playerId + "-" + 3 + "-" + Cache.getInstance().getTempUnitId(playerId));
		}
		return result;
	}
	
	private void init()
	{
		player.init();
		initCardGroup();
	}
	
	@SuppressWarnings("unchecked")
	private void initCardGroup()
	{
		Card[] cards = new Card[6];
		Skill[] skills = new Skill[6];
		List<PassiveSkill>[] pSkills = new List[6];
		List<Equip>[] equips = new List[6];
		int unitId = 0;
		
		// 角色卡
		if (formation.cardInfo != null && !"".equals(formation.cardInfo.trim()))
		{
			String[] ss = formation.cardInfo.split("-");
			for (int i = 0; i < ss.length; i++)
			{
				if (!ss[i].equals("n"))
				{
					cards[i] = this.cards.get(StringUtil.getInt(ss[i]));
				}
			}
		}
		// skill
		if (formation.skillInfo != null && !"".equals(formation.skillInfo.trim()))
		{
			String[] ss = formation.skillInfo.split("-");
			for (int i = 0; i < ss.length; i++)
			{
				if (!ss[i].equals("n"))
				{
					skills[i] = this.skills.get(StringUtil.getInt(ss[i]));
				}
			}
		}
		// passiveSkill
		if (formation.passiveSkillInfo != null && !"".equals(formation.passiveSkillInfo.trim()))
		{
			String[] ss = formation.passiveSkillInfo.split("-");
			for (int i = 0; i < ss.length; i++)
			{
				if (!ss[i].equals("n"))
				{
					pSkills[i] = new ArrayList<PassiveSkill>();
					String[] temps = ss[i].split("&");
					for (String temp : temps)
					{
						if (!temp.equals("x"))
						{
							pSkills[i].add(this.passiveSkills.get(StringUtil.getInt(temp)));
						}
						else
						{
							pSkills[i].add(null);
						}
						
					}
				}
			}
		}
		// equip
		if (formation.equipInfo != null && !"".equals(formation.equipInfo.trim()))
		{
			String[] ss = formation.equipInfo.split("-");
			for (int i = 0; i < ss.length; i++)
			{
				if (!ss[i].equals("n"))
				{
					equips[i] = new ArrayList<Equip>();
					String[] temps = ss[i].split("&");
					for (String temp : temps)
					{
						equips[i].add(this.equips.get(StringUtil.getInt(temp)));
					}
				}
			}
		}
		// 专属被动技能
		// unitSkill
		unitId = formation.unitSkillInfo;
		cg = new CardGroup(cards, skills, pSkills, equips, unitId);
	}
	
	public void build()
	{
		calBattlePower();
		buildFormation();
		player.build();
	}
	
	private void buildFormation()
	{
		// card
		String cardInfo = "";
		for (int i = 0; i < cg.cards.length; i++)
		{
			Card c = cg.cards[i];
			if (c != null)
			{
				cardInfo += this.getCards().indexOf(c) + "-";
			}
			else
			{
				cardInfo += "n-";
			}
		}
		if (!"".equals(cardInfo))
		{
			cardInfo = cardInfo.substring(0, cardInfo.length() - 1);
		}
		// skill
		String skillInfo = "";
		for (int i = 0; i < cg.skills.length; i++)
		{
			Skill s = cg.skills[i];
			if (s != null)
			{
				skillInfo += this.getSkills().indexOf(s) + "-";
			}
			else
			{
				skillInfo += "n-";
			}
		}
		if (!"".equals(skillInfo))
		{
			skillInfo = skillInfo.substring(0, skillInfo.length() - 1);
		}
		// pSkill
		String pSkillInfos = "";
		for (int i = 0; i < cg.pSkills.length; i++)
		{
			List<PassiveSkill> ps = cg.pSkills[i];
			if (ps != null && ps.size() > 0)
			{
				String temp = "";
				for (PassiveSkill p : ps)
				{
					if (p != null)
					{
						temp += this.getPassiveSkillls().indexOf(p) + "&";
					}
					else
					{
						temp += "x" + "&";
					}
				}
				if (!"".equals(temp))
				{
					temp = temp.substring(0, temp.length() - 1);
				}
				pSkillInfos += temp + "-";
			}
			else
			{
				pSkillInfos += "n-";
			}
		}
		if (!"".equals(pSkillInfos))
		{
			pSkillInfos = pSkillInfos.substring(0, pSkillInfos.length() - 1);
		}
		// equip
		String equipInfos = "";
		for (int i = 0; i < cg.equips.length; i++)
		{
			List<Equip> list = cg.equips[i];
			if (list != null && list.size() > 0)
			{
				String temp = "";
				for (Equip e : list)
				{
					temp += this.getEquips().indexOf(e) + "&";
				}
				if (!"".equals(temp))
				{
					temp = temp.substring(0, temp.length() - 1);
				}
				equipInfos += temp + "-";
			}
			else
			{
				equipInfos += "n-";
			}
		}
		if (!"".equals(equipInfos))
		{
			equipInfos = equipInfos.substring(0, equipInfos.length() - 1);
		}
		// 设置
		formation.cardInfo = cardInfo;
		formation.skillInfo = skillInfo;
		formation.passiveSkillInfo = pSkillInfos;
		formation.equipInfo = equipInfos;
		formation.unitSkillInfo = cg.unitId;
	}
	
	public void removeMails()
	{
		playerLogger.info("|区服：" + Cache.getInstance().serverId + "|玩家：" + player.getId() + "|" + player.getName() + "|等级：" + player.getLevel() + "|删除邮件|");
		long now = System.currentTimeMillis();
		for (Mail mail : mails)
		{
			if (mail.getState() != 1 && now - mail.getSendTime() >= mail.getDeleteTime() * 60 * 1000)
			{
				mail.setState(1);
			}
		}
	}
	
	public void calBattlePower()
	{
		int battlePower = 0;
		for (int i = 0; i < cg.cards.length; i++)
		{
			Card c = cg.cards[i];
			if (c != null)
			{
				int skillId = 0;
				int skillLevel = 0;
				Skill s = cg.skills[i];
				if (s != null)
				{
					skillId = s.getSkillId();
					skillLevel = s.getLevel();
				}
				else
				{
					skillId = CardData.getData(c.getCardId()).basicskill;
					skillLevel = 1;
				}
				List<PassiveSkill> ps = cg.pSkills[i];
				List<Integer> passiveSkillIds = new ArrayList<Integer>();
				if (ps != null && ps.size() > 0)
				{
					for (PassiveSkill p : ps)
					{
						if (p != null)
						{
							passiveSkillIds.add(p.getPassiveSkillId());
						}
					}
				}
				List<String> equipInfos = null;
				List<Equip> equips = cg.equips[i];
				if (equips != null)
				{
					equipInfos = new ArrayList<String>();
					for (int k = 0; k < equips.size(); k++)
					{
						Equip e = equips.get(k);
						equipInfos.add(e.getEquipId() + "-" + e.getLevel());
					}
				}
				battlePower += Statics.getCardPower(this, c, skillId, skillLevel, passiveSkillIds, equipInfos, cg, true);
			}
		}
		player.setBattlePower(battlePower);
		// 玩家战力达到n发奖励
		GmSpecialMailUtil.getInstance().playerBattlePowerSpeciaMail(player);
	}
	
	/**
	 * 获取挑战次数 lt@2014-5-10 下午09:22:24
	 * 
	 * @param missionId
	 * @return
	 */
	public int getFightTimes(int missionId)
	{
		int result = 0;
		MissionData md = MissionData.getMissionData(missionId);
		int sequence = md.getSequence();
		if (md.missiontype == 1)
		{
			String times = player.getTimes1();
			int beginIndex = (sequence - 1) * 2;
			int endIndex = beginIndex + 2;
			result = StringUtil.getInt(times.substring(beginIndex, endIndex));
		}
		else
		{
			String times = player.getTimes2();
			int beginIndex = (sequence - 1) * 2;
			int endIndex = beginIndex + 2;
			result = StringUtil.getInt(times.substring(beginIndex, endIndex));
		}
		return result;
	}
	
	public void gc()
	{
		session = null;
		player = null;
		cards.clear();
		cards = null;
		skills.clear();
		skills = null;
		passiveSkills.clear();
		passiveSkills = null;
		equips.clear();
		equips = null;
		items.clear();
		items = null;
		formation = null;
		if (cg != null)
		{
			cg.gc();
			cg = null;
		}
		if (friends != null)
		{
			friends.clear();
			friends = null;
		}
		if (lastRefreshList != null)
		{
			lastRefreshList.clear();
			lastRefreshList = null;
		}
		blrj = null;
		mblrj = null;
		brj = null;
		mbrj = null;
		pbrj = null;
		pblrj = null;
		ebrj = null;
		eblrj = null;
		erj = null;
		bJson = null;
		buyJson = null;
		pkcdtime = 0;
		mazecdtime = 0;
		eventcdtime1 = 0;
		eventcdtime2 = 0;
		eventcdtime3 = 0;
		newunitskills = "";
		if (mails != null)
		{
			mails.clear();
			mails = null;
		}
	}
	
	/** 已经解锁的合成物品 **/
	public HashMap<String, String> getComposeMap(PlayerInfo pi)
	{
		HashMap<String, String> comMap = new HashMap<String, String>();
		String[] temp = pi.player.getCompose().split(",");
		for (int i = 0; i < temp.length; i++)
		{
			if (temp[i] != null && temp[i].length() > 0)
			{
				if (!comMap.containsKey(temp[i]))
				{
					comMap.put(temp[i], temp[i]);
				}
			}
		}
		return comMap;
	}
	
	/** 获取卡牌的map key为星级，value为星级卡牌的数量 **/
	public HashMap<Integer, Integer> getCardNumMap()
	{
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int i = 0; i < getCards().size(); i++)
		{
			CardData cd = CardData.getData(getCards().get(i).getCardId());
			if (cd.element == 5 || cd.race == 6 || cd.race == 7)// 经验卡或者潘多拉宝箱或者神格卡
			{
				continue;
			}
			else
			{
				if (map.containsKey(cd.star))
				{
					map.put(cd.star, map.get(cd.star) + 1);
				}
				else
				{
					map.put(cd.star, 1);
				}
			}
			
		}
		return map;
	}
	
	public HashMap<Integer, Integer> getSkillNumMap()
	{
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int i = 0; i < getSkills().size(); i++)
		{
			SkillData sd = SkillData.getData(getSkills().get(i).getSkillId());
			if (sd.exptype == 1)// 不包括经验卡
			{
				if (map.containsKey(sd.star))
				{
					map.put(sd.star, map.get(sd.star) + 1);
				}
				else
				{
					map.put(sd.star, 1);
				}
			}
		}
		return map;
	}
	
	public HashMap<Integer, Integer> getPskillNumMap()
	{
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int i = 0; i < getPassiveSkillls().size(); i++)
		{
			PassiveSkillData pd = PassiveSkillData.getData(getPassiveSkillls().get(i).getPassiveSkillId());
			if (map.containsKey(pd.star))
			{
				map.put(pd.star, map.get(pd.star) + 1);
			}
			else
			{
				map.put(pd.star, 1);
			}
		}
		return map;
	}
	
	public HashMap<Integer, Integer> getEquipNumMap()
	{
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int i = 0; i < getEquips().size(); i++)
		{
			EquipData ed = EquipData.getData(getEquips().get(i).getEquipId());
			if (map.containsKey(ed.star))
			{
				map.put(ed.star, map.get(ed.star) + 1);
			}
			else
			{
				map.put(ed.star, 1);
			}
		}
		return map;
	}
	
	public HashMap<Integer, Card> getCardMap()
	{
		HashMap<Integer, Card> map = new HashMap<Integer, Card>();
		for (Card c : getCards())
		{
			if (!map.containsKey(c.getCardId()))
			{
				map.put(c.getCardId(), c);
			}
		}
		return map;
	}
	
	// 兑换要消耗的物品
	public void exchange(int type, int dataId, int num)
	{
		int deleteNum = 0;
		switch (type)
		{
			case 3:// card
				List<Card> cards = getCards();
				for (Card c : cards)
				{
					if (!cg.haveCard(c) && c.getCardId() == dataId && deleteNum < num)
					{
						c.setSell(3);
						deleteNum++;
					}
					if (deleteNum >= num)
					{
						break;
					}
				}
				break;
			case 4:// skill
				List<Skill> skills = getSkills();
				for (Skill s : skills)
				{
					if (!cg.haveSkill(s) && s.getSkillId() == dataId && deleteNum < num)
					{
						s.setSell(3);
						deleteNum++;
					}
					if (deleteNum >= num)
					{
						break;
					}
				}
				break;
			case 5:// pSkill
				List<PassiveSkill> pSkills = getPassiveSkillls();
				for (PassiveSkill s : pSkills)
				{
					if (!cg.havePskill(s) && s.getPassiveSkillId() == dataId && deleteNum < num)
					{
						s.setSell(3);
						deleteNum++;
					}
					if (deleteNum >= num)
					{
						break;
					}
				}
				break;
			case 2:// equip
				List<Equip> es = getEquips();
				for (Equip e : es)
				{
					if (!cg.haveEquip(e) && e.getEquipId() == dataId && deleteNum < num)
					{
						e.setSell(3);
						deleteNum++;
					}
					if (deleteNum >= num)
					{
						break;
					}
				}
				break;
			case 1:// item
				removeItem(dataId, num);
				break;
		}
	}
	
	/** 判断玩家兑换所消耗的物品是否足够 **/
	public boolean checkExchangeNeedNum(int type, int dataId, int needNum)
	{
		int curNeedNum = 0;
		boolean is = false;
		switch (type)
		{
			case 3:// card
				List<Card> cards = getCards();
				for (Card c : cards)
				{
					if (!cg.haveCard(c) && c.getCardId() == dataId)
					{
						curNeedNum++;
					}
				}
				if (curNeedNum >= needNum)
				{
					is = true;
				}
				break;
			case 4:// skill
				List<Skill> skills = getSkills();
				for (Skill s : skills)
				{
					if (!cg.haveSkill(s) && s.getSkillId() == dataId)
					{
						curNeedNum++;
					}
				}
				if (curNeedNum >= needNum)
				{
					is = true;
				}
				break;
			case 5:// pSkill
				List<PassiveSkill> pSkills = getPassiveSkillls();
				for (PassiveSkill s : pSkills)
				{
					if (!cg.havePskill(s) && s.getPassiveSkillId() == dataId)
					{
						curNeedNum++;
					}
				}
				if (curNeedNum >= needNum)
				{
					is = true;
				}
				break;
			case 2:// equip
				List<Equip> es = getEquips();
				for (Equip e : es)
				{
					if (!cg.haveEquip(e) && e.getEquipId() == dataId)
					{
						curNeedNum++;
					}
				}
				if (curNeedNum >= needNum)
				{
					is = true;
				}
				break;
			case 1:// item
				List<Item> it = getItems();
				for (Item i : it)
				{
					if (i.getItemId() == dataId)
					{
						curNeedNum += i.getPile();
					}
				}
				if (curNeedNum >= needNum)
				{
					is = true;
				}
				break;
		}
		return is;
	}
	
	// 玩家购买商城物品的每日次数
	public HashMap<Integer, Integer> getBuyShopNumMap()
	{
		String buyShopNum = player.getBuyShopNum();
		HashMap<Integer, Integer> shopMap = new HashMap<Integer, Integer>();
		if (buyShopNum != null && buyShopNum.length() > 0)
		{
			String[] ss = buyShopNum.split("-");
			for (int i = 0; i < ss.length; i++)
			{
				if (ss[i] != null && ss[i].length() > 0)
				{
					String[] temp = ss[i].split("&");
					if (!shopMap.containsKey(StringUtil.getInt(temp[0])))
					{
						shopMap.put(StringUtil.getInt(temp[0]), StringUtil.getInt(temp[1]));
					}
				}
			}
		}
		return shopMap;
	}
	
	// 玩家购买商城物品的总次数
	public HashMap<Integer, Integer> getBuyShopNumsMap()
	{
		String buyShopNums = player.getBuyShopNums();
		HashMap<Integer, Integer> shopMap = new HashMap<Integer, Integer>();
		if (buyShopNums != null && buyShopNums.length() > 0)
		{
			String[] ss = buyShopNums.split("-");
			for (int i = 0; i < ss.length; i++)
			{
				if (ss[i] != null && ss[i].length() > 0)
				{
					String[] temp = ss[i].split("&");
					if (!shopMap.containsKey(StringUtil.getInt(temp[0])))
					{
						shopMap.put(StringUtil.getInt(temp[0]), StringUtil.getInt(temp[1]));
					}
				}
			}
		}
		return shopMap;
	}
	
	/** 得到新卡时候判断是不是有新的合体技解锁 **/
	public void getNewUnitSkill(List<Integer> cardIds)
	{
		if (cardIds.size() > 0)
		{
			// 如果该卡有组成合体技 并且之前未得到过，则将其记录
			for (int n = 0; n < cardIds.size(); n++)
			{
				// 如果该卡有组成合体技 并且之前未得到过，则将其记录
				List<UnitSkillData> uDatas = UnitSkillData.getUnitSkillDatasByCard(cardIds.get(n), player.getUnitskills());
				if (uDatas != null && uDatas.size() > 0)
				{
					if (player.getOwncards() != null && player.getOwncards().length() > 0)
					{
						if (!player.getOwncards().contains(cardIds.get(n) + ""))
						{
							player.setOwncards(player.getOwncards() + "&" + cardIds.get(n));
						}
					}
					else
					{
						player.setOwncards(cardIds.get(n) + "");
					}
				}
				// 判断是否有新的合体技解锁
				String unit = "";
				for (int j = 0; j < uDatas.size(); j++)
				{
					UnitSkillData uData = uDatas.get(j);
					if (uData != null)
					{
						int m1 = 0;
						for (int k = 0; k < uData.cards.length; k++)
						{
							if (uData.cards[k] != 0)
							{
								if (player.getOwncards().contains(uData.cards[k] + ""))
								{
									m1++;
								}
								else
								{
									break;
								}
							}
						}
						if (m1 == uData.cardnum)
						{
							if (unit != null && unit.length() > 0)
							{
								unit = unit + "&" + uData.index;
							}
							else
							{
								unit = uData.index + "";
							}
							if (player.getUnitskills() != null && player.getUnitskills().length() > 0)
							{
								player.setUnitskills(player.getUnitskills() + "&" + uData.index);
							}
							else
							{
								player.setUnitskills(uData.index + "");
							}
						}
					}
				}
				if (this.newunitskills != null && this.newunitskills.length() > 0)
				{
					if (unit != null && unit.length() > 0)
					{
						this.newunitskills = this.newunitskills + "&" + unit;
					}
					
				}
				else
				{
					this.newunitskills = unit;
				}
			}
		}
		
	}
	
	/** 每个档位抽奖次数的map **/
	public HashMap<Integer, Integer> getLottoTimesMap()
	{
		HashMap<Integer, Integer> lMap = new HashMap<Integer, Integer>();
		if (player.getLottoTimes() != null && player.getLottoTimes().length() > 0)
		{
			String[] str = player.getLottoTimes().split("&");
			for (int i = 0; i < str.length; i++)
			{
				if (str[i] != null && str[i].length() > 0)
				{
					String[] temp = str[i].split("-");
					if (!lMap.containsKey(StringUtil.getInt(temp[0])))
					{
						lMap.put(StringUtil.getInt(temp[0]), StringUtil.getInt(temp[1]));
					}
				}
			}
		}
		return lMap;
	}
	
	/** 每个档位上次抽奖奖品的map ***/
	public HashMap<Integer, String> getLastLottoAwardMap()
	{
		HashMap<Integer, String> lastMap = new HashMap<Integer, String>();
		if (player.getLastLottoAward() != null && player.getLastLottoAward().length() > 0)
		{
			String[] str = player.getLastLottoAward().split("&");
			for (int i = 0; i < str.length; i++)
			{
				if (str[i] != null && str[i].length() > 0)
				{
					String[] temp = str[i].split("-");
					if (!lastMap.containsKey(StringUtil.getInt(temp[0])))
					{
						lastMap.put(StringUtil.getInt(temp[0]), temp[1] + "-" + temp[2]);
					}
				}
			}
		}
		return lastMap;
	}
	
	public CopyOnWriteArrayList<Maze> getMazes()
	{
		return mazes;
	}
	
	public void setMazes(CopyOnWriteArrayList<Maze> mazes)
	{
		this.mazes = mazes;
	}
	
	public CardJson[] getBattleData(int mid)
	{
		CardJson[] cjs = null;
		if (mazes != null && mazes.size() > 0)
		{
			for (Maze maze : mazes)
			{
				if (mid == maze.getMazeId())
				{
					
					String[] cardInfo = maze.getCardInfo().split(",");
					String[] skillInfo = maze.getSkillInfo().split(",");
					String[] passiveskillInfo = maze.getPassiveskillInfo().split(",");
					String[] equipInfo = maze.getEquipInfo().split(",");
					String[] talentInfo = maze.getTalentInfo().split(",");
					String[] breakInfo = maze.getBreakInfo().split("-");
					cjs = new CardJson[cardInfo.length];
					for (int i = 0; i < cjs.length; i++)
					{
						int cardId = 0;
						int level = 0;
						int skillId = 0;
						int skillLevel = 0;
						int breakNum = 0;
						int talent1 = 0;
						int talent2 = 0;
						int talent3 = 0;
						List<String> passiveSkillIds = new ArrayList<String>();
						List<String> equipInfos = new ArrayList<String>();
						if (maze.getCardInfo() != null && !"".equals(maze.getCardInfo()))
						{
							String[] s = cardInfo[i].split("-");
							cardId = StringUtil.getInt(s[0]);
							level = StringUtil.getInt(s[1]);
						}
						else
						{
							continue;
						}
						if (maze.getSkillInfo() != null && !"".equals(maze.getSkillInfo()))
						{
							String[] s = skillInfo[i].split("-");
							skillId = StringUtil.getInt(s[0]);
							skillLevel = StringUtil.getInt(s[1]);
						}
						else
						{
							skillId = CardData.getData(cardId).basicskill;
							skillLevel = 1;
						}
						if (maze.getPassiveskillInfo() != null && !"".equals(maze.getPassiveskillInfo()))
						{
							String[] s = passiveskillInfo[i].split("-");
							passiveSkillIds.add(s[0] + "");
							passiveSkillIds.add(s[1] + "");
							passiveSkillIds.add(s[2] + "");
						}
						if (maze.getEquipInfo() != null && !"".equals(maze.getEquipInfo()))
						{
							String[] s = equipInfo[i].split("-");
							for (int j = 0; j < s.length; j++)
							{
								if (j%2==0)
								{
									equipInfos.add(s[j] + "-" + s[j+1]);
								}
							}
//							equipInfos.add(s[0] + "-" + s[1]);
//							equipInfos.add(s[2] + "-" + s[3]);
//							equipInfos.add(s[4] + "-" + s[5]);
						}
						if (maze.getBreakInfo() != null && !"".equals(maze.getBreakInfo()))
						{
							breakNum = StringUtil.getInt(breakInfo[i]);
						}
						if (maze.getTalentInfo() != null && !"".equals(maze.getTalentInfo()))
						{
							String[] talents = talentInfo[i].split("-");
							talent1 = StringUtil.getInt(talents[0]);
							talent2 = StringUtil.getInt(talents[1]);
							talent3 = StringUtil.getInt(talents[2]);
						}
						cjs[i] = new CardJson(i, cardId, level, skillId, skillLevel, passiveSkillIds, equipInfos, breakNum, talent1, talent2, talent3);
					}
				}
			}
		}
		return cjs;
	}
}
