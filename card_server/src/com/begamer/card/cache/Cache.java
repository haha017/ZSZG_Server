package com.begamer.card.cache;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.begamer.card.common.Constant;
import com.begamer.card.common.dao.hibernate.CommDao;
import com.begamer.card.common.util.StringUtil;
import com.begamer.card.common.util.binRead.CornucopiaData;
import com.begamer.card.common.util.binRead.DailyTaskData;
import com.begamer.card.common.util.binRead.EventData;
import com.begamer.card.common.util.binRead.FriendCostData;
import com.begamer.card.common.util.binRead.IntegralFirstData;
import com.begamer.card.common.util.binRead.RankRobotData;
import com.begamer.card.common.util.binRead.ShoppvpData;
import com.begamer.card.common.util.binRead.UnLockData;
import com.begamer.card.common.util.binRead.UniteskillrobotData;
import com.begamer.card.controller.PkController;
import com.begamer.card.json.ErrorJson;
import com.begamer.card.log.MemLogger;
import com.begamer.card.model.dao.ActivityInfoDao;
import com.begamer.card.model.dao.TimeMailDao;
import com.begamer.card.model.pojo.Activity;
import com.begamer.card.model.pojo.ActivityInfo;
import com.begamer.card.model.pojo.Announce;
import com.begamer.card.model.pojo.Card;
import com.begamer.card.model.pojo.Event;
import com.begamer.card.model.pojo.EventDrop;
import com.begamer.card.model.pojo.Friend;
import com.begamer.card.model.pojo.GameBox;
import com.begamer.card.model.pojo.LogBuy;
import com.begamer.card.model.pojo.LotRank;
import com.begamer.card.model.pojo.Mail;
import com.begamer.card.model.pojo.PkRank;
import com.begamer.card.model.pojo.Player;
import com.begamer.card.model.pojo.SpeciaMail;
import com.begamer.card.model.pojo.TimeMail;
import com.begamer.card.model.pojo.Var;

/**
 * 缓存:存放player
 * 
 * @author LiTao 2013-12-13 上午10:34:03
 */
public class Cache implements Runnable {
	private static final Logger logger = MemLogger.logger;
	private static boolean running;
	public static Cache instance;
	/** 游戏服Id **/
	public int serverId;
	/** 游戏服name **/
	public String serverName;
	/** 激活码服务器地址 **/
	public String gift_code_server;
	/** 获取支付订单号url **/
	public String pay_order_url;
	/** 支付密钥 **/
	public String pay_game_key;
	/** 请求次数* */
	public static int requestNum;
	/** 错误次数* */
	public static int errorNum;
	/** 缓存中的玩家* */
	private ConcurrentHashMap<Integer, PlayerInfo> playerInfos = new ConcurrentHashMap<Integer, PlayerInfo>();
	/** 开服时间* */
	public static long startServerTime;
	/** 登录次数* */
	private static long loginTime;
	/** 最大在线人数* */
	private static long maxOnlineNum;
	/** 打印日志时间间隔* */
	private static long MaxPrintLogTime = 2 * 60 * 1000;
	private long lastPrintLogTime;
	/** 最大删除无效数据间隔* */
	private static long MaxDeleteInvalidDataTime = 1 * 24 * 3600 * 1000;
	private long lastDeleteTime;
	/** 最大处理playerinfo间隔* */
	private static long MaxProcessTime = 20 * 60 * 1000;
	private long lastProcessTime;
	public static int MaxSize;
	/** 上次0点操作日期* */
	public static String lastZeroDate;
	/** 最大好友申请有效时间* */
	private static long MaxApplyValidTime = 7 * 24 * 3600 * 1000;
	/** 最大验证申请时间* */
	private static long MaxCheckApplyTime = 10 * 60 * 1000;
	/** 上次验证好友申请时间* */
	private long lastCheckApplysTime;
	/** 最大验证玩家信息时间* */
	private static long MaxCheckPiTime = 60 * 1000;
	/** 上次验证玩家信息时间* */
	private long lastCheckPiTime;
	/** 最大处理pkrank的时间* */
	private static long MaxSavePkRankTime = 20 * 3600 * 1000;
	/** 上次保存pkrank的时间* */
	private static long lastSavePkRankTime;
	/** 所有玩家等级,每1小时从数据库查一次* */
	private HashMap<Integer, String> playerTemps = new HashMap<Integer, String>();
	/** 上次同步玩家等级时间* */
	private long lastSelectPlayerLevelsTime;
	/** 同步玩家等级时间间隔* */
	private static long SelectPlayerLevelsTime = 30 * 60 * 1000;
	/** 最大删除邮件间隔* */
	private static long MaxRemoveMailTime = 2 * 3600 * 1000;
	/** 上次删除邮件间隔* */
	private long lastRemoveMailTime;
	/** 上次留存开始的时间 **/
	public long lastRetentionTime;
	/** 执行删除公告时间 **/
	public long MaxRemoveAnnounces = 30 * 1000;
	/** 上次删除公告时间 **/
	public long lastRemoveAnnounces;
	/** 上次系统刷新时间 **/
	private long lastPvpRefreshTime;
	/** 本时段pvp商城道具 **/
	private String pvpShopItems;
	/** 服务器发送/接收总的字节数* */
	private static long sendByteNum;
	private static long receiveByteNum;
	private static long beforeGzipEncriptSendByteNum;
	private static long afterGzipEncriptSendByteNum;
	
	@Autowired
	private CommDao commDao;
	@Autowired
	private TimeMailDao timeMailDao;
	@Autowired
	private ActivityInfoDao activityInfoDao;
	
	/** pk排名* */
	private ConcurrentHashMap<Integer, PkRank> pkRanks = new ConcurrentHashMap<Integer, PkRank>();
	/** 活动副本* */
	public List<Event> events = new ArrayList<Event>();
	
	/** 运营活动 **/
	public CopyOnWriteArrayList<Activity> activitys = new CopyOnWriteArrayList<Activity>();
	
	/** 配置的运营活动详细信息 **/
	public CopyOnWriteArrayList<ActivityInfo> activityInfos = new CopyOnWriteArrayList<ActivityInfo>();
	/** 好友申请集合* */
	private CopyOnWriteArrayList<Friend> applyFriends = new CopyOnWriteArrayList<Friend>();
	/** 经验倍率* */
	private static int expMul;
	/** 推图掉落倍率* */
	private static int missionDropMul;
	/** 迷宫掉落倍率* */
	private static int mazeDropMul;
	/** 金币掉落概率 **/
	private static int goldDropMul;
	/** 活动副本与掉落倍率* */
	private static HashMap<Integer, Float> eventDropMulMap = new HashMap<Integer, Float>();
	/** 游戏内公告 **/
	private static CopyOnWriteArrayList<Announce> announces = new CopyOnWriteArrayList<Announce>();
	/** 每天最大接收体力次数 **/
	public static int maxPowerTimes = 15;
	/** 每次收取好友体力值 **/
	public static int friendPower = 2;
	/** 全局掉落:普通关卡 **/
	public static List<EventDrop> eventDropsForNormal;
	/** 全局掉落:精英关卡 **/
	public static List<EventDrop> eventDropsForSpecial;
	/** 全局掉落:迷宫 **/
	public static List<EventDrop> eventDropsForMaze;
	
	public static HashMap<Integer, GameBox> gameBoxsMap = new HashMap<Integer, GameBox>();
	/** 抽卡排行榜 **/
	private ConcurrentHashMap<Integer, LotRank> lotRanks = new ConcurrentHashMap<Integer, LotRank>();
	// 积分排行榜
	public List<String> lotRanksView = new ArrayList<String>();
	/** 最大处理抽卡排行榜的时间* */
	private static long MaxSavelotRankTime = 30 * 60 * 1000;
	/** 上次保存抽卡排行榜的时间* */
	private static long lastSavelotRankTime;
	
	/** 添加公告 **/
	public boolean addAnnounce(Announce announce)
	{
		boolean is = true;
		if (announce == null)
		{
			return false;
		}
		if (announce.getType() == 2)
		{
			for (int i = 0; i < announces.size(); i++)
			{
				Announce a = announces.get(i);
				if (a.getId() == announce.getId())
				{
					is = false;
					break;
				}
			}
		}
		if (is)
		{
			announces.add(announce);
		}
		return is;
	}
	
	/** 删除 **/
	public void delAnnounce(int id)
	{
		for (int i = 0; i < announces.size(); i++)
		{
			Announce announce = announces.get(i);
			if (announce.getId() == id)
			{
				announces.remove(i);
				break;
			}
		}
	}
	
	/** 修改公告 **/
	public void upAnnounce(Announce announce)
	{
		if (announce == null)
		{
			return;
		}
		for (int i = 0; i < announces.size(); i++)
		{
			Announce a = announces.get(i);
			if (a.getId() == announce.getId())
			{
				announces.remove(i);
				announces.add(announce);
				break;
			}
		}
	}
	
	/** 获取公告 **/
	public List<Announce> getAnnounces()
	{
		List<Announce> list = new ArrayList<Announce>();
		list.addAll(announces);
		return list;
	}
	
	/** 获取在线玩家数* */
	public int getOnLinePlayer()
	{
		return playerInfos.size();
	}
	
	/** 设置活动副本掉落倍率* */
	public static void setEventDropMulMap(int id, float mul)
	{
		if (mul < 1f)
		{
			return;
		}
		eventDropMulMap.put(id, mul);
		EventData eventData = EventData.getEventData(id);
		logger.info("设置活动副本掉落倍率:" + eventData.name + ":" + mul);
	}
	
	public static HashMap<Integer, Float> getEventDropMulMap()
	{
		return eventDropMulMap;
	}
	
	/** 初始化eventDropMulMap，加载至缓存* */
	private void initEventDropMulMap()
	{
		List<EventData> eventList = EventData.getEvents();
		for (EventData eventData : eventList)
		{
			eventDropMulMap.put(eventData.ID, 1f);
		}
	}
	
	public static int getMazeDropMul()
	{
		return mazeDropMul;
	}
	
	/** 设置迷宫掉落倍率* */
	public static void setMazeDropMul(int mul)
	{
		if (mul < 1f)
		{
			return;
		}
		mazeDropMul = mul;
		logger.info("设置迷宫掉落倍率:" + mul);
	}
	
	public static int getGoldDropMul()
	{
		return goldDropMul;
	}
	
	public static void setGoldDropMul(int mul)
	{
		if (mul < 1f)
		{
			return;
		}
		goldDropMul = mul;
		logger.info("设置金币掉落倍率：" + mul);
	}
	
	/** 设置经验倍率* */
	public static void setExpMul(int mul)
	{
		if (mul < 1f)
		{
			return;
		}
		expMul = mul;
		logger.info("设置经验倍率:" + mul);
	}
	
	public static int getExpMul()
	{
		return expMul;
	}
	
	/** 设置掉落倍率* */
	public static void setMissionDropMul(int mul)
	{
		if (mul < 1f)
		{
			return;
		}
		missionDropMul = mul;
		logger.info("设置推图掉落倍率:" + mul);
	}
	
	
	public static int getMaxSize()
	{
		return MaxSize;
	}

	public static void setMaxSize(int maxSize)
	{
		if (maxSize<0)
		{
			return;
		}
		MaxSize = maxSize;
		logger.info("设置游戏服最大在线人数："+ maxSize);
	}

	public static int getMissionDropMul()
	{
		return missionDropMul;
	}
	
	public static void recordRequestNum(ErrorJson ej)
	{
		requestNum++;
		if (ej.errorCode != 0)
		{
			errorNum++;
		}
	}
	
	/** 记录字节数,type:1发送,2接收,3发送加密压缩前的字节数,4发送加密压缩后的字节数* */
	public static synchronized void recordFlowInfo(int type, int num)
	{
		if (type == 1)
		{
			sendByteNum += num;
		}
		else if (type == 2)
		{
			receiveByteNum += num;
		}
		else if (type == 3)
		{
			beforeGzipEncriptSendByteNum += num;
		}
		else if (type == 4)
		{
			afterGzipEncriptSendByteNum += num;
		}
	}
	
	public void initTimeMail()
	{
		List<TimeMail> list = timeMailDao.isOpen();
		if (list.size() == 0 || list == null)
		{
			return;
		}
		for (TimeMail timeMail : list)
		{
			MailThread.getInstance().addTimeMail(timeMail);
		}
		logger.info("初始化已开启的定时邮件:" + list.size());
	}
	
	public void initSpecialMail()
	{
		List<SpeciaMail> list = commDao.getSpeciaMailsBindTime();
		if (list.size() == 0 || list == null)
		{
			return;
		}
		for (SpeciaMail speciaMail : list)
		{
			MailThread.getInstance().addSpeciaMail(speciaMail);
		}
		logger.info("初始化已开启的特殊定时邮件:" + list.size());
	}
	
	/** 关服* */
	public void closeCache()
	{
		// 保存玩家信息
		for (PlayerInfo playerInfo : playerInfos.values())
		{
			savePlayerInfo(playerInfo);
		}
		// 保存玩家pkRank信息
		commDao.savePkRanks(pkRanks.values());
		// 保存玩家抽卡排行信息
		commDao.saveLotRanks(lotRanks.values());
		// 保存好友体力和倍率信息
		List<Var> vars = commDao.getVars();
		if (vars != null && vars.size() > 0)
		{
			vars.get(0).setVar(maxPowerTimes);
			vars.get(1).setVar(friendPower);
			vars.get(2).setVar(expMul);
			vars.get(3).setVar(missionDropMul);
			vars.get(4).setVar(mazeDropMul);
			vars.get(5).setVar(goldDropMul);
			vars.get(6).setVar(MaxSize);
			commDao.saveVars(vars);
		}
	}
	
	public void initCache()
	{
		if (instance == null)
		{
			instance = this;
		}
		List<Var> vars = commDao.getVars();
		if (vars != null && vars.size() > 0)
		{
			/** 每天最大接收体力次数 **/
			maxPowerTimes = vars.get(0).var;
			/** 每次收取好友体力值 **/
			friendPower = vars.get(1).var;
			// 经验倍率
			expMul = vars.get(2).var;
			// 推图掉落倍率
			missionDropMul = vars.get(3).var;
			// 迷宫掉落倍率
			mazeDropMul = vars.get(4).var;
			// 金币掉落倍率
			goldDropMul = vars.get(5).var;
			//游戏服最大在线人数
			MaxSize = vars.get(6).var;
		}
		else
		{
			/** 每天最大接收体力次数 **/
			maxPowerTimes = 15;
			/** 每次收取好友体力值 **/
			friendPower = 2;
			// 经验倍率
			expMul = 1;
			// 推图掉落倍率
			missionDropMul = 1;
			// 迷宫掉落倍率
			mazeDropMul = 1;
			// 金币掉落倍率
			goldDropMul = 1;
			//游戏服最大人数
			MaxSize = 1600;
		}
		// 活动副本掉落倍率
		initEventDropMulMap();
		initEventDrop();
		
		lastProcessTime = System.currentTimeMillis();
		startServerTime = System.currentTimeMillis();
		lastZeroDate = StringUtil.getDate(System.currentTimeMillis());
		lastSavePkRankTime = System.currentTimeMillis();
		initPlayer();
		initPkRank();
		initLotRanks();
		initEvent();
		initActivity();
		initActivityInfo();
		MailThread.getInstance();
		initTimeMail();
		initSpecialMail();
		running = true;
		new Thread(instance).start();
	}
	
	public static Cache getInstance()
	{
		return instance;
	}
	
	public void run()
	{
		logger.info("启动缓存线程");
		while (running)
		{
			try
			{
				long time = System.currentTimeMillis();
				try
				{
					if (pvpShopCanRefresh(time))
					{
						pvpShopItems = ShoppvpData.getRandomDatas(0);
						for (PlayerInfo pi : playerInfos.values())
						{
							if (null != pi && pi.player.getLastPvpShopRefreshTime() != lastPvpRefreshTime)
							{
								pi.player.setLastPvpShopRefreshTime(lastPvpRefreshTime);
								pi.player.setPvpShop(pvpShopItems);
							}
						}
					}
				}
				catch (Exception e)
				{
					logger.error("系统定时刷新pvp商城道具错误！", e);
				}
				// 0点操作
				String zeroDate = StringUtil.getDate(time);
				if (!lastZeroDate.equals(zeroDate))
				{
					logger.info("0点操作开始");
					lastZeroDate = zeroDate;
					// 零点清空每天有全局数量限制的宝箱随机物品
					for (GameBox gBox : gameBoxsMap.values())
					{
						if (gBox.getGamebox_time() == 1)
						{
							gameBoxsMap.remove(gBox);
						}
					}
					for (PlayerInfo pi : playerInfos.values())
					{
						zeroProcess(pi, lastZeroDate);
						// 零点--清空冷却时间
						pi.pkcdtime = 0;
						pi.mazecdtime = 0;
						pi.eventcdtime1 = 0;
						pi.eventcdtime2 = 0;
						pi.eventcdtime3 = 0;
					}
					for (Event e : events)
					{
						zeroProcessEvent(e);
					}
					// 刷新全局掉落
					initEventDrop();
					// 大风车活动结束，清空数据
					Activity act = getActivityById(8);
					if (act != null)
					{
						long endTime = StringUtil.getTimeStamp(act.getEndtime() + " 23:59:59");
						if (System.currentTimeMillis() - endTime >= 0)
						{
							List<Integer>[] list = getAllPlayerIds();
							for (int playerId : list[0])
							{
								PlayerInfo pi = Cache.getInstance().getPlayerInfoForGm(playerId);
								if ("".equals(pi.player.getLastLottoAward()) || "".equals(pi.player.getLottoReward()) || "".equals(pi.player.getLottoTimes()) || "".equals(pi.player.getLottoTurnTimes()))
								{
									pi.player.setLastLottoAward("");
									pi.player.setLottoReward("");
									pi.player.setLottoTimes("");
									pi.player.setLottoTurnTimes("");
								}
							}
							for (int playerId : list[1])
							{
								Player player = getPlayer(playerId);
								if ("".equals(player.getLastLottoAward()) || "".equals(player.getLottoReward()) || "".equals(player.getLottoTimes()) || "".equals(player.getLottoTurnTimes()))
								{
									commDao.delActiviDFC(playerId);
								}
							}
							logger.info("大风车活动结束，清空玩家数据!");
						}
					}
					// 限时神将奖励
					Activity a = getActivityById(9);
					if (a != null)
					{
						if (System.currentTimeMillis() - StringUtil.getTimeStamp(a.getEndtime() + " 23:59:59") >= 0)
						{
							if (lotRanks.values() != null)
							{
								for (LotRank lr : lotRanks.values())
								{
									IntegralFirstData ifd = IntegralFirstData.getData(lr.getRank());
									if (ifd != null)
									{
										sendLotRankMail(lr.getPlayerId(), ifd);
									}
									else 
									{
										ifd = IntegralFirstData.getData(30000);
										if (ifd != null)
										{
											sendLotRankMail(lr.getPlayerId(), ifd);
										}
									}
									
								}
								lotRanksView.clear();
								lotRanks.clear();
								commDao.delLotRanks();
								logger.info("限时神将活动结束，清空玩家数据!");
							}
						}
					}
					//清空玩家迷宫进度
					commDao.delMaze();
					logger.info("0点操作结束");
				}
				// 好友申请有效时间验证
				if (time - lastCheckApplysTime >= MaxCheckApplyTime)
				{
					lastCheckApplysTime = time;
					for (Friend f : applyFriends)
					{
						if (time - StringUtil.getTimeStamp(f.getAddDate()) >= MaxApplyValidTime)
						{
							applyFriends.remove(f);
						}
					}
				}
				// 定期保存玩家信息
				if (time - lastProcessTime >= MaxProcessTime)
				{
					lastProcessTime = time;
					long timeTemp = System.currentTimeMillis();
					for (PlayerInfo pi : playerInfos.values())
					{
						savePlayerInfo(pi);
					}
					logger.info("定期保存玩家信息用时:" + (System.currentTimeMillis() - timeTemp) + "ms");
				}
				// 定期保存排行榜pkrank信息,event
				if (time - lastSavePkRankTime >= MaxSavePkRankTime)
				{
					lastSavePkRankTime = time;
					commDao.savePkRanks(pkRanks.values());
					for (Event event : events)
					{
						saveEvent(event);
					}
				}
				// 过期的删掉
				if (time - lastCheckPiTime >= MaxCheckPiTime)
				{
					lastCheckPiTime = time;
					for (PlayerInfo pi : playerInfos.values())
					{
						if (!pi.isValid())
						{
							pi.player.setLogoutTime(StringUtil.getDateTime(System.currentTimeMillis()));
							savePlayerInfo(pi);
							playerInfos.remove(pi.player.getId());
							pi.gc();
						}
					}
				}
				// 定期删除数据库无效数据
				if (time - lastDeleteTime >= MaxDeleteInvalidDataTime)
				{
					lastDeleteTime = time;
					String deleteMsg = deletePlayerInfo();
					logger.info("删除数据库无效数据:" + deleteMsg);
				}
				// 定期同步玩家等级信息
				if (time - lastSelectPlayerLevelsTime >= SelectPlayerLevelsTime)
				{
					lastSelectPlayerLevelsTime = time;
					HashMap<Integer, String> temps = commDao.getPlayerTemps();
					int num = temps.size();
					if (temps.size() > 0)
					{
						playerTemps = temps;
					}
					logger.info("同步玩家等级:" + num);
				}
				// 定期删除过期邮件
				if (time - lastRemoveMailTime >= MaxRemoveMailTime)
				{
					lastRemoveMailTime = time;
					for (PlayerInfo pi : playerInfos.values())
					{
						pi.removeMails();
					}
					int num = commDao.removeMails();
					logger.info("定期删除过期邮件:" + num);
				}
				// 定期清除跑马灯公告
				if (time - lastRemoveAnnounces >= MaxRemoveAnnounces)
				{
					lastRemoveAnnounces = time;
					for (int i = announces.size() - 1; i >= 0; i--)
					{
						Announce e = announces.get(i);
						int subTime = StringUtil.getSubSecond(e.getTime(), time);
						if (subTime >= (e.getFrequency() * e.getNum() * 60))
						{
							announces.remove(e);
						}
					}
					logger.info("定期清除公告,剩余:" + announces.size());
				}
				// 定期保存抽卡排行信息
				if (time - lastSavelotRankTime >= MaxSavelotRankTime)
				{
					lastSavelotRankTime = time;
					commDao.saveLotRanks(lotRanks.values());
					initLotRanks();
				}
				// 打印日志
				if (time - lastPrintLogTime >= MaxPrintLogTime)
				{
					lastPrintLogTime = time;
					logger.info("(发送)压缩加密前大小:" + beforeGzipEncriptSendByteNum / 1024 + "KB");
					logger.info("(发送)压缩加密后大小:" + afterGzipEncriptSendByteNum / 1024 + "KB");
					logger.info("已发送" + sendByteNum / 1024 + "KB");
					logger.info("已接收" + receiveByteNum / 1024 + "KB");
					if (playerInfos.size() > maxOnlineNum)
					{
						maxOnlineNum = playerInfos.size();
					}
					logger.info("最大同时在线人数:" + maxOnlineNum + ",当前缓存有玩家:" + playerInfos.size());
					logger.info((time - startServerTime) / 1000 / 60 + "分内登录次数:" + loginTime);
					long total = Runtime.getRuntime().totalMemory() / 1024 / 1024;
					long used = (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024 / 1024;
					long free = Runtime.getRuntime().freeMemory() / 1024 / 1024;
					logger.info("内存共有空间:" + total + "MB,已用:" + used + "MB,剩余:" + free + "MB");
					logger.info("请求次数:" + requestNum + ",出错次数:" + errorNum + ",出错比率:" + (((float) errorNum) / requestNum));
				}
				Thread.sleep(1000);
			}
			catch (Exception e)
			{
				logger.error("缓存线程出错", e);
			}
		}
		logger.info("缓存线程关闭");
	}
	
	private boolean savePlayerInfo(PlayerInfo pi)
	{
		return commDao.savePlayerInfo(pi);
	}
	
	private PlayerInfo getDbPlayerInfo(int playerId)
	{
		return commDao.getPlayerInfo(playerId);
	}
	
	// 删除无效数据
	private String deletePlayerInfo()
	{
		return commDao.deletePlayerInfo();
	}
	
	/**
	 * 获取玩家 lt@2014-1-3 下午04:03:10
	 * 
	 * @param playerId
	 * @param session
	 * @param isLogin
	 * @return
	 */
	public PlayerInfo getPlayerInfo(int playerId, HttpSession session)
	{
		PlayerInfo pi = playerInfos.get(playerId);
		if (pi != null && pi.getSession() == session)
		{
			pi.player.setLastResponseTime(StringUtil.getDateTime(System.currentTimeMillis()));
			return pi;
		}
		return null;
	}
	
	public PlayerInfo getPlayerInfoForGm(int playerId)
	{
		return playerInfos.get(playerId);
	}
	
	public PlayerInfo getPlayerInfoByMemory(int playerId)
	{
		return playerInfos.get(playerId);
	}
	
	/**
	 * 登录 lt@2014-1-3 下午04:06:53
	 * 
	 * @param playerId
	 * @param session
	 * @return
	 */
	public PlayerInfo login(int playerId, HttpSession session)
	{
		PlayerInfo pi = null;
		pi = playerInfos.get(playerId);
		if (pi != null)
		{
			pi.setSession(session);
		}
		else
		{
			if (playerInfos.size() >= MaxSize)
			{
				return null;
			}
			// 从数据库中取
			pi = getDbPlayerInfo(playerId);
			if (pi != null)
			{
				pi.setSession(session);
				playerInfos.put(pi.player.getId(), pi);
				pi.pkcdtime = 0;
				pi.mazecdtime = 0;
				pi.eventcdtime1 = 0;
				pi.eventcdtime2 = 0;
				pi.eventcdtime3 = 0;
				pi.newunitskills = "";
				if (pi.player.getLineGiftTime() == null || pi.player.getLineGiftTime().length() == 0)
				{
					pi.player.setLineGiftTime(StringUtil.getDateTime(System.currentTimeMillis()));
				}
			}
		}
		// 获取该玩家的pkrank,若不再缓存中，将其加至缓存
		if (!pkRanks.containsKey(playerId))
		{
			PkRank pr = PkRank.getNewPkRank(playerId);
			pr.setPlayerLevel(pi.player.getLevel());
			pkRanks.put(pi.player.getId(), pr);
		}
		
		String thisLoginTime = StringUtil.getDateTime(System.currentTimeMillis());
		// 设置上次登录时间
		if (!StringUtil.isSameDay(pi.player.getLastLogin(), thisLoginTime))
		{
			pi.player.addLoginDayNum();
			pi.player.setLineGiftTime(StringUtil.getDateTime(System.currentTimeMillis()));
			pi.player.setBlackMarketRefreshTime("");
		}
		pi.player.setLastLogin(thisLoginTime);
		// 0点操作
		if (!pi.player.getLastZeroDate().equals(lastZeroDate))
		{
			zeroProcess(pi, lastZeroDate);
		}
		// 计算体力
		if (pi.player.getPower() < Constant.MaxPower)
		{
			long lastRestorePowerTime = pi.player.getLastRestorePowerTime();
			long num = (System.currentTimeMillis() - lastRestorePowerTime) / Constant.AutoRestorePowerTime;
			if (num > 0)
			{
				pi.player.setLastRestorePowerTime(System.currentTimeMillis());
			}
			pi.player.addPower((int) num, true);
		}
		/** 设置每日任务状态的值 **/
		List<DailyTaskData> dts = DailyTaskData.getDailyTaskDatas();
		if (pi.player.getDailyTaskState() == null || pi.player.getDailyTaskState().length() <= 0)
		{
			for (int i = 0; i < dts.size(); i++)
			{
				
				if (dts.get(i) != null)
				{
					if (dts.get(i).request == 0 && pi.player.getVipLevel() > 0)// 玩家为vip时，扫荡券任务状态为1，否则为0，在充值vip时，判断状态如果为0则改为1
					{
						pi.player.setDailyTaskState(pi.player.getDailyTaskState() + 1);
					}
					else
					{
						pi.player.setDailyTaskState(pi.player.getDailyTaskState() + 0);
					}
				}
			}
		}
		if (pi.player.getDailyTaskState().length() < dts.size())
		{
			for (int i = 0; i < dts.size() - pi.player.getDailyTaskState().length(); i++)
			{
				pi.player.setDailyTaskState(pi.player.getDailyTaskState() + 0);
			}
		}
		else if (pi.player.getDailyTaskState().length() > dts.size())
		{
			for (int i = 0; i < 2 * dts.size() - pi.player.getDailyTaskState().length(); i++)
			{
				pi.player.setDailyTaskState(pi.player.getDailyTaskState() + 0);
			}
		}
		loginTime++;
		// 把背包中的卡加入到曾经获得的卡中
		// if(pi.player.getOwncards()==null ||
		// pi.player.getOwncards().length()==0)
		// {
		List<Integer> cardIds = new ArrayList<Integer>();
		for (Card c : pi.getCards())
		{
			if (!cardIds.contains(c.getCardId()))
			{
				cardIds.add(c.getCardId());
			}
		}
		if (cardIds.size() > 0)
		{
			pi.getNewUnitSkill(cardIds);
		}
		// }
		return pi;
	}
	
	/** 0点操作* */
	private void zeroProcess(PlayerInfo pi, String lastZeroDate)
	{
		pi.player.setLastZeroDate(lastZeroDate);
		pi.player.setMaze("");// 迷宫进入次数
		pi.player.setPowerTimes(0);
		pi.mazes.clear();
		/** 重置为未赠不可收状态* */
		for (Friend f : pi.getFriends())
		{
			f.setState(0);
		}
		// 活动副本进入次数清空数据
		clearFbnum(pi.player);
		// 清除pve挑战次数
		pi.player.clearTimes1();
		pi.player.clearTimes2();
		// 签到
		long curTimeStamp = System.currentTimeMillis();
		long lastSignTimeStamp = StringUtil.getTimeStamp(pi.player.getLastSignTime());
		if (!StringUtil.isSameMonth(curTimeStamp, lastSignTimeStamp))
		{
			pi.player.setSignTimes(0);
		}
		// 清空购买金币和体力和pvp进入次数的购买的次数
		pi.player.setBuyGoldTimes(0);
		pi.player.setBuyPowerTimes(0);
		pi.player.setBuyPkNumTimes(0);
		// pve进入次数购买
		pi.player.clearBuyPveEntryTimes1();
		pi.player.clearBuyPveEntryTimes2();
		
		// 清空今日邀请好友战斗次数
		pi.player.setInviteFriendTimes(0);
		
		// 清空每日任务的进度以及初始每日任务的状态
		List<DailyTaskData> dts = DailyTaskData.getDailyTaskDatas();
		String state = "";
		for (int i = 0; i < dts.size(); i++)
		{
			
			if (dts.get(i) != null)
			{
				if (dts.get(i).request == 0 && pi.player.getVipLevel() > 0)// 玩家为vip时，扫荡券任务状态为1，否则为0，在充值vip时，判断状态如果为0则改为1
				{
					state = state + "1";
				}
				else
				{
					state = state + "0";
				}
			}
		}
		pi.player.setPvpShopRefreshTimes(0);
		pi.player.setActive(0);
		pi.player.initActiveState();
		pi.player.setDailyTaskState(state);
		pi.player.setDailyTaskComplete("");
		pi.player.setLineGift("");
		pi.player.setLineGiftTime(StringUtil.getDateTime(System.currentTimeMillis()));
		PkRank pr = pkRanks.get(pi.player.getId());
		if (pr != null)
		{
			if (pr.getPlayerLevel() != pi.player.getLevel())
			{
				pr.setPlayerLevel(pi.player.getLevel());
			}
			sendRewardMail(pr);// 发送奖励邮件
			pr.setZeroTime(lastZeroDate);
			pr.setPkNum(0);
			pr.setPvpAward(0);
			pr.setRankAwardType(0);
			pr.setEntryType(0);
			pr.setWinNum(0);
		}
		// 商城有日购买次数限制的物品购买信息
		pi.player.setBuyShopNum("");
		pi.player.setBuyRefreshTimes(0);
		
		// 聚宝盆回馈礼包
		if (pi.player.getCornucopia() > 0)
		{
			Activity a = getActivityById(6);
			if (pi.player.getCornucopiaMail() <= 7)
			{
				if (System.currentTimeMillis() - StringUtil.getTimeStamp(a.getEndtime() + " 00:00:00") >= 0)
				{
					CornucopiaSendMail(pi.player, a);
				}
			}
			if (pi.player.getCornucopiaMail() > 7)// 如果聚宝盆活动介乎。反馈奖励已经发完，则将聚宝盆的数据清空
			{
				pi.player.setCornucopiaMail(0);
				pi.player.setCornucopiaCrystal(0);
				pi.player.setCornucopia(0);
			}
		}
	}
	
	// ===================援护玩家使用 start==============================//
	public Set<Integer> getTempPlayerKeys()
	{
		return playerTemps.keySet();
	}
	
	public int getTempLevel(int playerId)
	{
		String s = playerTemps.get(playerId);
		if (s != null)
		{
			String[] ss = s.split("-");
			int level = StringUtil.getInt(ss[0]);
			return level;
		}
		else
		{
			return 0;
		}
	}
	
	public int getTempUnitId(int playerId)
	{
		String s = playerTemps.get(playerId);
		if (s != null)
		{
			String[] ss = s.split("-");
			int unitId = StringUtil.getInt(ss[1]);
			return unitId;
		}
		else
		{
			return 0;
		}
	}
	
	// ===================援护玩家使用 end================================//
	
	/** 添加申请* */
	public void addFriendApply(Friend fid)
	{
		if (fid != null)
		{
			applyFriends.add(fid);
		}
	}
	
	/** 删除申请* */
	public void removeApply(Friend f)
	{
		if (f != null)
		{
			applyFriends.remove(f);
		}
	}
	
	/** 获取我的申请* */
	public List<Friend> getMyApplys(int playerId)
	{
		List<Friend> result = new ArrayList<Friend>();
		for (Friend f : applyFriends)
		{
			if (f.getPlayerId() == playerId)
			{
				result.add(f);
			}
		}
		return result;
	}
	
	/** 获取申请我* */
	public List<Friend> getApplyMes(int playerId)
	{
		List<Friend> result = new ArrayList<Friend>();
		for (Friend f : applyFriends)
		{
			if (f.getFriendId() == playerId)
			{
				result.add(f);
			}
		}
		return result;
	}
	
	/** 获取玩家* */
	public Player getPlayer(int playerId)
	{
		PlayerInfo pi = playerInfos.get(playerId);
		if (pi != null)
		{
			return pi.player;
		}
		else
		{
			return commDao.getPlayer(playerId);
		}
	}
	
	/**
	 * 获取好友个数 lt@2014-3-4 下午03:17:36
	 * 
	 * @param playerId
	 * @return
	 */
	public boolean canAddFriend(int playerId)
	{
		PlayerInfo pi = playerInfos.get(playerId);
		if (pi != null)
		{
			int buyFriendTimes = pi.player.getBuyFriendTimes();
			FriendCostData fd = FriendCostData.getData(buyFriendTimes);
			if (fd == null)
			{
				return false;
			}
			else
			{
				return pi.getFriends().size() < fd.number1;
			}
		}
		else
		{
			Player dbPlayer = commDao.getPlayer(playerId);
			if (dbPlayer == null)
			{
				return false;
			}
			else
			{
				int buyFriendTimes = dbPlayer.getBuyFriendTimes();
				FriendCostData fd = FriendCostData.getData(buyFriendTimes);
				if (fd == null)
				{
					return false;
				}
				else
				{
					int friendNum = commDao.getFriendNum(playerId);
					return friendNum < fd.number1;
				}
			}
		}
	}
	
	/**
	 * 添加好友 lt@2014-3-3 下午05:00:04
	 * 
	 * @param playerId
	 * @param friendId
	 */
	public void addFriend(int playerId, int friendId)
	{
		PlayerInfo pi = playerInfos.get(playerId);
		Friend f = new Friend();
		f.setPlayerId(playerId);
		f.setFriendId(friendId);
		f.setState(0);
		f.setLastHelpDay("");
		f.setAddDate(StringUtil.getDateTime(System.currentTimeMillis()));
		if (pi != null)
		{
			pi.addFriend(f);
		}
		else
		{
			commDao.saveFriend(f);
		}
	}
	
	/**
	 * 添加好友 lt@2014-3-3 下午05:00:04
	 * 
	 * @param playerId
	 * @param friendId
	 */
	public void removeFriend(int playerId, int friendId)
	{
		PlayerInfo pi = playerInfos.get(playerId);
		if (pi != null)
		{
			List<Friend> list = pi.getFriends();
			for (int k = list.size() - 1; k >= 0; k--)
			{
				Friend f = list.get(k);
				if (f.getFriendId() == friendId)
				{
					f.setState(1);
					break;
				}
			}
		}
		else
		{
			commDao.removeFriend(playerId, friendId);
		}
	}
	
	public Player searchPlayer(String name)
	{
		for (PlayerInfo pi : playerInfos.values())
		{
			if (pi.player.getName().equals(name.trim()))
			{
				return pi.player;
			}
		}
		return commDao.getPlayer(name);
	}
	
	/** 赠送好友体力* */
	public void sendPower(int playerId, int friendId)
	{
		PlayerInfo pi = playerInfos.get(playerId);
		if (pi != null)
		{
			List<Friend> list = pi.getFriends();
			for (Friend f : list)
			{
				if (f.getFriendId() == friendId)
				{
					// 好友状态:0未赠不可收,1已删除,2未赠可收,3未赠已收,4已赠不可收,5已赠可收,6已赠已收
					if (f.getState() == 0)
					{
						f.setState(2);
					}
					if (f.getState() == 4)
					{
						f.setState(5);
					}
					break;
				}
			}
		}
		else
		{
			List<String> states = new ArrayList<String>();
			states.add("0-2");
			states.add("4-5");
			commDao.updateFriend(playerId, friendId, states);
		}
	}
	
	/** 根据用户id获取玩家信息* */
	public PlayerInfo getPlayerInfo(int playerId)
	{
		PlayerInfo pInfo = playerInfos.get(playerId);
		if (pInfo == null)
		{
			pInfo = getDbPlayerInfo(playerId);
		}
		return pInfo;
	}
	
	/** 获取最末排名* */
	public int getMaxRank()
	{
		int rank1 = 0;
		for (PkRank pr : pkRanks.values())
		{
			if (pr.getRank() > rank1)
			{
				rank1 = pr.getRank();
			}
			
		}
		
		return rank1;
	}
	
	private void initPlayer()
	{
		logger.info("开始注册初始玩家");
		HashMap<Integer, String> temps = commDao.getPlayerTemps();
		if (temps.size() == 0)
		{
			List<UniteskillrobotData> list = UniteskillrobotData.getDatas();
			commDao.regUniteskillrobotPlayers(list);
		}
		logger.info("注册初始玩家完毕");
	}
	
	/** 初始化pkranks，加载至缓存* */
	private void initPkRank()
	{
		logger.info("开始注册初始排行");
		List<PkRank> pkRanksTemp = commDao.getPkRanks();
		if (pkRanksTemp == null || pkRanksTemp.size() == 0)
		{
			List<RankRobotData> list = RankRobotData.getDatas();
			commDao.regRankRobotPlayers(list);
			List<Integer> playerIds = commDao.getPlayerIdsForInitRank(0);
			int rank = 0;
			for (int k = Constant.InitPlayerNum; k < playerIds.size(); k++)
			{
				int playerLevel = list.get(rank).level;
				rank++;
				int playerId = playerIds.get(k);
				pkRanks.put(playerId, PkRank.getNewPkRank(playerId, playerLevel, rank));
			}
			logger.info("插入排行数据");
			commDao.savePkRanks(pkRanks.values());
			logger.info("插入排行数据完毕");
		}
		else
		{
			for (PkRank pr : pkRanksTemp)
			{
				pkRanks.put(pr.getPlayerId(), pr);
			}
		}
		logger.info("注册初始排行完毕");
	}
	
	/** pkrankMap* */
	public PkRank getRankById(int playerId)
	{
		return pkRanks.get(playerId);
	}
	
	public HashMap<Integer, PkRank> getPkRankMap2()
	{
		HashMap<Integer, PkRank> map = new HashMap<Integer, PkRank>();
		for (PkRank pr : pkRanks.values())
		{
			if (!map.containsKey(pr.rank))
			{
				map.put(pr.rank, pr);
			}
		}
		return map;
	}
	
	/** 根据排名获取pkrank* */
	public PkRank getPkRankByRank(int rank)
	{
		return getPkRankMap2().get(rank);
	}
	
	/** 加载event表* */
	private void initEvent()
	{
		events = commDao.getEvents();
	}
	
	/** 活动副本map* */
	public HashMap<Integer, Event> getEventMap()
	{
		HashMap<Integer, Event> eMap = new HashMap<Integer, Event>();
		for (Event e : events)
		{
			if (e != null)
			{
				eMap.put(e.getEventId(), e);
			}
		}
		return eMap;
	}
	
	/** 保存event* */
	public void saveEvent(Event e)
	{
		commDao.saveEvent(e);
	}
	
	/** 零点操作-判断副本开启或结束,并修改标识* */
	private void zeroProcessEvent(Event e)
	{
		String now = StringUtil.getToday();
		// 当前时间既不是开启时间，也不是结束时间，并且也不在两者之间
		if ((!e.getSt().equals(now)) && (!e.getEnd().equals(now)) && (!StringUtil.compareTime(e.getSt(), e.getEnd())))
		{
			e.setSell(0);
		}
		else
		{
			if (e.getSt().equals(now) || StringUtil.compareTime(e.getSt(), e.getEnd()))// 开启时间，或在时间段内
			{
				e.setSell(1);
			}
			if (e.getEnd().equals(now))// 结束时间
			{
				e.setSell(0);
			}
		}
	}
	
	/** 零点清空副本进入次数* */
	private void clearFbnum(Player p)
	{
		p.setFBnum1("");
		if (p.getFBnum2() != null && p.getFBnum2().length() > 0)
		{
			String[] str = p.getFBnum2().split(",");
			HashMap<Integer, Event> eMap = getEventMap();
			for (int k = 0; k < str.length; k++)
			{
				if (str[k] != null && str[k].length() > 0)
				{
					String[] temp = str[k].split("-");
					if (eMap.get(StringUtil.getInt(temp[0])).getSell() == 0)
					{
						str[k] = "";
					}
				}
			}
			String fbNum = "";
			for (String s : str)
			{
				if (s != null && s.length() > 0)
				{
					fbNum = fbNum + s + ",";
				}
			}
			p.setFBnum2(fbNum);
		}
	}
	
	public void sendMail(Mail mail)
	{
		if (mail == null)
		{
			return;
		}
		int playerId = mail.getPlayerId();
		PlayerInfo pi = playerInfos.get(playerId);
		if (pi != null)
		{
			pi.addMail(mail);
		}
		else
		{
			commDao.saveMail(mail);
		}
	}
	
	/**
	 * 获取全服玩家(level>=4) lt@2014-4-25 下午09:03:44
	 * 
	 * @return 第一个元素为在线,第二个元素为不在线
	 */
	@SuppressWarnings("unchecked")
	public List<Integer>[] getAllPlayerIds()
	{
		long time = System.currentTimeMillis();
		int minLevel = 4;
		List<Integer> playerIds = commDao.getPlayerIds(minLevel);
		for (int playerId : playerInfos.keySet())
		{
			PlayerInfo pi = playerInfos.get(playerId);
			if (pi != null && pi.player.getLevel() >= minLevel && !playerIds.contains(playerId))
			{
				playerIds.add(playerId);
			}
		}
		// 在线不在线的分开
		List<Integer> online = new ArrayList<Integer>();
		List<Integer> offline = new ArrayList<Integer>();
		for (int playerId : playerIds)
		{
			if (playerInfos.get(playerId) != null)
			{
				online.add(playerId);
			}
			else
			{
				offline.add(playerId);
			}
		}
		logger.info("获取全服玩家用时:" + (System.currentTimeMillis() - time) + "ms");
		List[] result = new ArrayList[2];
		result[0] = online;
		result[1] = offline;
		return result;
	}
	
	/**
	 * 获取全服玩家(level>=17)1完成3场竞技场 lt@2014-5-10 下午19:20:44
	 * 
	 * @return 第一个元素为在线,第二个元素为不在线
	 */
	@SuppressWarnings("unchecked")
	public List<Integer>[] getAllThreePkPlayerIds()
	{
		long time = System.currentTimeMillis();
		UnLockData uData = UnLockData.getUnlockByMode(9);
		int minLevel = uData.method;
		// 所有等级>=17的玩家
		List<Integer> playerIds = commDao.getPlayerIds(minLevel);
		for (int playerId : playerInfos.keySet())
		{
			PlayerInfo pi = playerInfos.get(playerId);
			if (pi != null && pi.player.getLevel() >= minLevel && !playerIds.contains(playerId))
			{
				playerIds.add(playerId);
			}
		}
		// 等级>=17并且pknum>=3的玩家
		List<Integer> pids = new ArrayList<Integer>();
		for (int pid : playerIds)
		{
			PkRank pr = pkRanks.get(pid);
			if (pr != null)
			{
				if (pr.getPkNum() >= 3)
				{
					pids.add(pid);
				}
			}
		}
		// 在线不在线的分开
		List<Integer> online = new ArrayList<Integer>();
		List<Integer> offline = new ArrayList<Integer>();
		for (int playerId : pids)
		{
			if (playerInfos.get(playerId) != null)
			{
				online.add(playerId);
			}
			else
			{
				offline.add(playerId);
			}
		}
		logger.info("获取全服完成3场竞技场的玩家用时:" + (System.currentTimeMillis() - time) + "ms");
		List[] result = new ArrayList[2];
		result[0] = online;
		result[1] = offline;
		return result;
	}
	
	/**
	 * 获取全服玩家(level>=17)2获得3场竞技场胜利 lt@2014-5-10 下午19:20:44
	 * 
	 * @return 第一个元素为在线,第二个元素为不在线
	 */
	@SuppressWarnings("unchecked")
	public List<Integer>[] getAllThreePkWinPlayerIds()
	{
		long time = System.currentTimeMillis();
		UnLockData uData = UnLockData.getUnlockByMode(9);
		int minLevel = uData.method;
		// 所有等级>=17的玩家
		List<Integer> playerIds = commDao.getPlayerIds(minLevel);
		for (int playerId : playerInfos.keySet())
		{
			PlayerInfo pi = playerInfos.get(playerId);
			if (pi != null && pi.player.getLevel() >= minLevel && !playerIds.contains(playerId))
			{
				playerIds.add(playerId);
			}
		}
		// 等级>=17并且winnum>=3的玩家
		List<Integer> pids = new ArrayList<Integer>();
		for (int pid : playerIds)
		{
			PkRank pr = pkRanks.get(pid);
			if (pr != null)
			{
				if (pr.getWinNum() >= 3)
				{
					pids.add(pid);
				}
			}
		}
		// 在线不在线的分开
		List<Integer> online = new ArrayList<Integer>();
		List<Integer> offline = new ArrayList<Integer>();
		for (int playerId : pids)
		{
			if (playerInfos.get(playerId) != null)
			{
				online.add(playerId);
			}
			else
			{
				offline.add(playerId);
			}
		}
		logger.info("获取全服完成3场竞技场胜利的玩家用时:" + (System.currentTimeMillis() - time) + "ms");
		List[] result = new ArrayList[2];
		result[0] = online;
		result[1] = offline;
		return result;
	}
	
	public void batchSaveMail(List<Mail> mails)
	{
		commDao.batchSaveMail(mails);
	}
	
	/** pvp邮件发送奖励* */
	public void sendRewardMail(PkRank pr)
	{
		int rankAward = 0;
		int honor = 0;
		if (pr.getRank() != 0)
		{
			rankAward = PkController.rankAward(pr.getPlayerLevel(), pr);
			honor = PkController.rankAward1(pr.getPlayerLevel(), pr);
			Mail mail = Mail.createMail(pr.getPlayerId(), "竞技场管理员", "每日排名奖励", "您的昨日排名是：" + pr.rank, "", "", "", "", "", "", 0, 0, rankAward, 0, 0, Constant.MailKeepTime3);
			mail.setHonor(honor);
			sendMail(mail);
		}
	}
	
	/** 加载活动 **/
	public void initActivity()
	{
		List<Activity> temp = commDao.getActivitys();
		if (activitys != null && activitys.size() > 0)
		{
			activitys.clear();
		}
		activitys.addAll(temp);
	}
	
	public List<Activity> getActivitys()
	{
		List<Activity> list = new ArrayList<Activity>();
		if (activitys != null && activitys.size() > 0)
		{
			for (Activity a : activitys)
			{
				if (a.getSell() == 0)
				{
					list.add(a);
				}
			}
		}
		return list;
	}
	
	/** 加载配置的活动 **/
	public void initActivityInfo()
	{
		List<ActivityInfo> temp = activityInfoDao.find2();
		activityInfos.addAll(temp);
		logger.info("初始化配置的活动内容：" + activityInfos.size());
	}
	
	public List<ActivityInfo> getActivityInfos()
	{
		List<ActivityInfo> aInfo = new ArrayList<ActivityInfo>();
		if (activityInfos != null && activityInfos.size() > 0)
		{
			for (ActivityInfo activityInfo : activityInfos)
			{
				if (activityInfo.getSell() == 0)
				{
					aInfo.add(activityInfo);
				}
			}
		}
		return aInfo;
	}
	
	// 获取兑换活动
	public List<ActivityInfo> getActivityInfosforType()
	{
		List<ActivityInfo> aInfo = new ArrayList<ActivityInfo>();
		if (activityInfos != null && activityInfos.size() > 0)
		{
			for (ActivityInfo activityInfo : activityInfos)
			{
				if (activityInfo.getSell() == 0 && activityInfo.getActivityType() == 4)
				{
					aInfo.add(activityInfo);
				}
			}
		}
		return aInfo;
	}
	
	/** 根据活动id获取活动信息 **/
	public List<ActivityInfo> getActivityInfosForId(int activityId)
	{
		List<ActivityInfo> aInfo = new ArrayList<ActivityInfo>();
		if (activityInfos != null && activityInfos.size() > 0)
		{
			for (ActivityInfo activityInfo : activityInfos)
			{
				if (activityInfo.getSell() == 0 && activityInfo.getActivityType() == 4 && activityInfo.getActivityId() == activityId)
				{
					aInfo.add(activityInfo);
				}
			}
		}
		return aInfo;
	}
	
	public void addActivityInfo(ActivityInfo activityInfo)
	{
		if (activityInfo == null)
		{
			return;
		}
		activityInfos.add(activityInfo);
		logger.info("添加配置的活动信息" + "|所属活动ID：" + activityInfo.getActivityId() + "|当前配置的活动内容：" + activityInfos.size());
	}
	
	public void delActivityInfo(int id)
	{
		if (id <= 0)
		{
			return;
		}
		for (int i = 0; i < activityInfos.size(); i++)
		{
			ActivityInfo activityInfo = activityInfos.get(i);
			if (activityInfo.getSell() == 0 && activityInfo.getId() == id)
			{
				activityInfos.remove(i);
				logger.info("删除配置的活动信息" + "|活动ID：" + activityInfo.getId() + "|当前配置的活动内容：" + activityInfos.size());
			}
		}
	}
	
	public void addActivity(Activity activity)
	{
		if (activity == null)
		{
			return;
		}
		activitys.add(activity);
		logger.info("添加运营活动" + "|活动ID：" + activity.getActivityId());
	}
	
	public void delActivity(int activityId)
	{
		if (activityId <= 0)
		{
			return;
		}
		for (int i = activityInfos.size() - 1; i >= 0; i--)
		{
			ActivityInfo activityInfo = activityInfos.get(i);
			if (activityInfo.getSell() == 0 && activityInfo.getActivityId() == activityId)
			{
				activityInfos.remove(i);
			}
		}
		for (int i = 0; i < activitys.size(); i++)
		{
			Activity activity = activitys.get(i);
			
			if (activity.getActivityId() == activityId && activity.getSell() == 0)
			{
				activitys.remove(i);
				break;
			}
		}
		logger.info("删除活动列表" + "|活动ID：" + activityId + "|当前配置的活动内容：" + activityInfos.size());
	}
	
	public void upActivity(Activity activity)
	{
		if (activity != null)
		{
			for (Activity e : activitys)
			{
				if (e.getId() == activity.getId())
				{
					activitys.remove(e);
					activitys.add(activity);
					break;
				}
			}
		}
	}
	
	public ActivityInfo getActivityInfoByid(int id)
	{
		ActivityInfo activityInfo = null;
		if (id <= 0)
		{
			return activityInfo;
		}
		else
		{
			for (ActivityInfo e : activityInfos)
			{
				if (e.getId() == id)
				{
					activityInfo = ActivityInfo.createActivityInfo(e.getActivityType(), e.getActivityId(), e.getName(), e.getContent(), e.getExchangeType(), e.getExchangeId(), e.getExchangeNum(), e.getNeedType(), e.getNeedId(), e.getNeedNum(), e.getNeedType2(), e.getNeedId2(), e.getNeedNum2(), e.getNeedType3(), e.getNeedId3(), e.getNeedNum3(), e.getSole(), e.getExchangeContext());
					activityInfo.setWeight(e.getWeight());
					activityInfo.setId(id);
				}
			}
			return activityInfo;
		}
	}
	
	public void upActivityInfo(ActivityInfo activityInfo)
	{
		if (activityInfo != null)
		{
			for (ActivityInfo e : activityInfos)
			{
				if (e.getId() == activityInfo.getId())
				{
					activityInfos.remove(e);
					activityInfos.add(activityInfo);
				}
			}
		}
	}
	
	public int getServerId()
	{
		return serverId;
	}
	
	public void setServerId(int serverId)
	{
		this.serverId = serverId;
	}
	
	public String getServerName()
	{
		return serverName;
	}
	
	public void setServerName(String serverName)
	{
		this.serverName = serverName;
	}
	
	public void setGift_code_server(String gift_code_server)
	{
		this.gift_code_server = gift_code_server;
	}
	
	public String getGift_code_server()
	{
		return gift_code_server;
	}
	
	public String getPay_order_url()
	{
		return pay_order_url;
	}
	
	public void setPay_order_url(String payOrderUrl)
	{
		pay_order_url = payOrderUrl;
	}
	
	public String getPay_game_key()
	{
		return pay_game_key;
	}
	
	public void setPay_game_key(String payGameKey)
	{
		pay_game_key = payGameKey;
	}
	
	// 聚宝盆奖励发送邮件
	public void CornucopiaSendMail(Player p, Activity a)
	{
		String todayStr = StringUtil.getDateTime(System.currentTimeMillis());
		int dayNum = (int) ((StringUtil.getTimeStamp(todayStr) - StringUtil.getTimeStamp(a.getEndtime() + " 00:00:00")) / (1 * 24 * 3600 * 1000));
		
		if (dayNum >= 7)
		{
			dayNum = 7 - p.getCornucopiaMail();
		}
		else
		{
			dayNum = dayNum - p.getCornucopiaMail();
		}
		if (p.getCornucopia() >= 0 && p.getCornucopia() <= 8)
		{
			CornucopiaData cData = CornucopiaData.getCornucopiaData(p.getCornucopia());
			int mailNum = p.getCornucopiaMail();
			for (int i = 0; i < dayNum; i++)
			{
				if (mailNum + i <= 6 && mailNum + i >= 0)
				{
					String str = cData.dayaward.get(i + mailNum);
					String[] temp = str.split("-");
					Mail mail = null;
					if (StringUtil.getInt(temp[0]) <= 5)
					{
						String[] ss = temp[1].split(",");
						mail = Mail.createMail(p.getId(), "GM", "聚宝盆反馈礼包", "", temp[0] + "&" + ss[0] + "&" + ss[1], "", "", "", "", "", 0, 0, 0, 0, 0, Constant.MailKeepTime1);
					}
					else
					{
						if (StringUtil.getInt(temp[0]) == 6)// 金币
						{
							mail = Mail.createMail(p.getId(), "GM", "聚宝盆反馈礼包", "", "", "", "", "", "", "", StringUtil.getInt(temp[1]), 0, 0, 0, 0, Constant.MailKeepTime1);
						}
						
						else if (StringUtil.getInt(temp[0]) == 8)// 水晶
						{
							mail = Mail.createMail(p.getId(), "GM", "聚宝盆反馈礼包", "", "", "", "", "", "", "", 0, StringUtil.getInt(temp[1]), 0, 0, 0, Constant.MailKeepTime1);
						}
						else if (StringUtil.getInt(temp[0]) == 9)// 符文
						{
							mail = Mail.createMail(p.getId(), "GM", "聚宝盆反馈礼包", "", "", "", "", "", "", "", 0, 0, StringUtil.getInt(temp[1]), 0, 0, Constant.MailKeepTime1);
						}
						else if (StringUtil.getInt(temp[0]) == 10)// 体力
						{
							mail = Mail.createMail(p.getId(), "GM", "聚宝盆反馈礼包", "", "", "", "", "", "", "", 0, 0, 0, StringUtil.getInt(temp[1]), 0, Constant.MailKeepTime1);
						}
						else if (StringUtil.getInt(temp[0]) == 11)// 友情
						{
							mail = Mail.createMail(p.getId(), "GM", "聚宝盆反馈礼包", "", "", "", "", "", "", "", 0, 0, 0, 0, StringUtil.getInt(temp[1]), Constant.MailKeepTime1);
						}
					}
					
					sendMail(mail);
					p.setCornucopiaMail(p.getCornucopiaMail() + 1);
				}
				if (mailNum + i == 6)
				{
					for (int k = 1; k <= p.getCornucopia(); k++)
					{
						CornucopiaData cData2 = CornucopiaData.getCornucopiaData(k);
						String str = cData2.dayaward.get(i + 1 + mailNum);
						String[] temp = str.split("-");
						Mail mail = null;
						if (StringUtil.getInt(temp[0]) <= 5)
						{
							String[] ss = temp[1].split(",");
							mail = Mail.createMail(p.getId(), "GM", "聚宝盆终极礼包", "", temp[0] + "&" + ss[0] + "&" + ss[1], "", "", "", "", "", 0, 0, 0, 0, 0, Constant.MailKeepTime1);
						}
						else
						{
							if (StringUtil.getInt(temp[0]) == 6)// 金币
							{
								mail = Mail.createMail(p.getId(), "GM", "聚宝盆终极礼包", "", "", "", "", "", "", "", StringUtil.getInt(temp[1]), 0, 0, 0, 0, Constant.MailKeepTime1);
							}
							
							else if (StringUtil.getInt(temp[0]) == 8)// 水晶
							{
								mail = Mail.createMail(p.getId(), "GM", "聚宝盆终极礼包", "", "", "", "", "", "", "", 0, StringUtil.getInt(temp[1]), 0, 0, 0, Constant.MailKeepTime1);
							}
							else if (StringUtil.getInt(temp[0]) == 9)// 符文
							{
								mail = Mail.createMail(p.getId(), "GM", "聚宝盆终极礼包", "", "", "", "", "", "", "", 0, 0, StringUtil.getInt(temp[1]), 0, 0, Constant.MailKeepTime1);
							}
							else if (StringUtil.getInt(temp[0]) == 10)// 体力
							{
								mail = Mail.createMail(p.getId(), "GM", "聚宝盆终极礼包", "", "", "", "", "", "", "", 0, 0, 0, StringUtil.getInt(temp[1]), 0, Constant.MailKeepTime1);
							}
							else if (StringUtil.getInt(temp[0]) == 11)// 友情
							{
								mail = Mail.createMail(p.getId(), "GM", "聚宝盆终极礼包", "", "", "", "", "", "", "", 0, 0, 0, 0, StringUtil.getInt(temp[1]), Constant.MailKeepTime1);
							}
						}
						sendMail(mail);
					}
					p.setCornucopiaMail(p.getCornucopiaMail() + 1);
				}
			}
		}
	}
	
	public void saveLogbuys(List<LogBuy> logs)
	{
		if (logs != null && logs.size() > 0)
		{
			commDao.saveLogbuys(logs);
		}
	}
	
	/** 初始化全局掉落 **/
	public void initEventDrop()
	{
		List<EventDrop> eds = commDao.findEventDrops();
		if (eds != null && eds.size() > 0)
		{
			List<EventDrop> edsForNormal = new ArrayList<EventDrop>();
			List<EventDrop> edsForSpecial = new ArrayList<EventDrop>();
			List<EventDrop> edsForMaze = new ArrayList<EventDrop>();
			String date = StringUtil.getDate(System.currentTimeMillis());
			for (EventDrop ed : eds)
			{
				if (date.compareTo(ed.getStartDay()) >= 0 && date.compareTo(ed.getEndDay()) <= 0)
				{
					// 普通关
					if (ed.getEventType() == 1 || ed.getEventType() == 2 || ed.getEventType() == 5)
					{
						edsForNormal.add(ed);
					}
					// 精英关
					if (ed.getEventType() == 1 || ed.getEventType() == 3 || ed.getEventType() == 5)
					{
						edsForSpecial.add(ed);
					}
					// 迷宫
					if (ed.getEventType() == 4 || ed.getEventType() == 5)
					{
						edsForMaze.add(ed);
					}
				}
			}
			eventDropsForNormal = edsForNormal;
			eventDropsForSpecial = edsForSpecial;
			eventDropsForMaze = edsForMaze;
		}
		logger.info("初始化全局掉落");
	}
	
	/** 根据活动id获取活动 **/
	public Activity getActivityById(int index)
	{
		for (Activity a : getActivitys())
		{
			if (a.getActivityId() == index)
			{
				return a;
			}
		}
		return null;
	}
	
	public long getLastPvpRefreshTime()
	{
		return lastPvpRefreshTime;
	}
	
	public void setLastPvpRefreshTime(long lastPvpRefreshTime)
	{
		this.lastPvpRefreshTime = lastPvpRefreshTime;
	}
	
	public String getPvpShopItems()
	{
		return pvpShopItems;
	}
	
	public void setPvpShopItems(String pvpShopItems)
	{
		this.pvpShopItems = pvpShopItems;
	}
	
	/**
	 * pvp商城系统刷新
	 * 
	 * @param time
	 * @return
	 */
	public boolean pvpShopCanRefresh(long time)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String ds0 = sdf.format(lastPvpRefreshTime);
		String ds1 = sdf.format(time);
		SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm");
		String[] ss = sdf1.format(new Date(time)).split(":");
		int hour = Integer.valueOf(ss[0]);
		int min = Integer.valueOf(ss[1]);
		if ((hour == 12 || hour == 18 || hour == 21) && min == 0 && !ds0.equals(ds1))
		{
			lastPvpRefreshTime = time;
			return true;
		}
		return false;
	}
	
	public void initLotRanks()
	{
		List<LotRank> lrs = commDao.getLotRanks();
		if (lrs != null && lrs.size() > 0)
		{
			lotRanksView.clear();
			for (int i = 0; i < lrs.size(); i++)
			{
				LotRank lr = lrs.get(i);
				lr.setRank(i + 1);
				lotRanks.put(lr.getPlayerId(), lr);
				if (lr.getRank() <= 5)
				{
					Player player = Cache.getInstance().getPlayer(lr.getPlayerId());
					lotRanksView.add(lr.getRank() + "-" + player.getName() + "-" + lr.getScore() + "-" + player.getId());
				}
				// System.out.println("...............................playerId:"
				// + lr.getPlayerId() + "|lotNum:" + lr.getLotNum() +
				// "|lotRank:" + lr.getRank() + "|lotScore" + lr.getScore());
			}
			logger.info("初始化抽卡活动排行!");
		}
	}
	
	public void addLotRanks(LotRank lr)
	{
		if (lr != null)
		{
			if (lotRanks.containsKey(lr.getPlayerId()))
			{
				lotRanks.replace(lr.getPlayerId(), lr);
			}
			else
			{
				lotRanks.put(lr.getPlayerId(), lr);
			}
		}
	}
	
	/** lotRankMap* */
	public LotRank getLotRankById(int playerId)
	{
		return lotRanks.get(playerId);
	}
	
	/** 根据排名获取lotRank* */
	public LotRank getLotRankMap(int rank)
	{
		return getLotRankMap2().get(rank);
	}
	
	public HashMap<Integer, LotRank> getLotRankMap2()
	{
		HashMap<Integer, LotRank> map = new HashMap<Integer, LotRank>();
		for (LotRank lr : lotRanks.values())
		{
			if (!map.containsKey(lr.getRank()))
			{
				map.put(lr.getRank(), lr);
			}
		}
		return map;
	}
	
	/** 限时神将发送奖励 **/
	public void sendLotRankMail(int pId, IntegralFirstData ifd)
	{
		LotRank lr = getLotRankById(pId);
		if (lr != null)
		{
			if (lr.getRank() <= ifd.pank && lr.getScore() >= ifd.integralvalue)
			{
				Mail mail = new Mail();
				String[] temp = ifd.rewards;
				String rewqrd1 = "";
				String rewqrd2 = "";
				String rewqrd3 = "";
				String rewqrd4 = "";
				String rewqrd5 = "";
				for (int j = 0; j < temp.length; j++)
				{
					String reward = temp[j];
					if (reward != null && reward.trim().length() > 0)
					{
						String[] rz = reward.split("&");
						int type = StringUtil.getInt(rz[0]);
						if (type == 1)
						{
							rewqrd1 = temp[j];
						}
						if (type == 2)
						{
							rewqrd2 = temp[j];
						}
						if (type == 3)
						{
							rewqrd3 = temp[j];
						}
						if (type == 4)
						{
							rewqrd4 = temp[j];
						}
						if (type == 5)
						{
							rewqrd5 = temp[j];
						}
						if (type == 6)
						{
							mail.setGold(StringUtil.getInt(rz[1]));
						}
						if (type == 8)
						{
							mail.setCrystal(StringUtil.getInt(rz[1]));
						}
						if (type == 9)
						{
							mail.setRuneNum(StringUtil.getInt(rz[1]));
						}
						if (type == 10)
						{
							mail.setPower(StringUtil.getInt(rz[1]));
						}
						if (type == 11)
						{
							mail.setFriendNum(StringUtil.getInt(rz[1]));
						}
						if (type == 12)
						{
							mail.setDiamond(StringUtil.getInt(rz[1]));
						}
					}
				}
				Mail mail2 = Mail.createMail(pId, "GM", ifd.mailtab, ifd.mailtext, rewqrd1, rewqrd2, rewqrd3, rewqrd4, rewqrd5, "", mail.getGold(), mail.getCrystal(), mail.getRuneNum(), mail.getPower(), mail.getFriendNum(), Constant.MailKeepTime1);
				mail2.setDiamond(mail.getDiamond());
				sendMail(mail2);
				logger.info("给" + pId + "发送限时神将活动奖励");
			}
		}
	}
}
